
                  <?php
                  if(@$create){
                    $reset = true;
                    $action = 'Create';
                    $role = '';
                  }else{
                    $reset = false;
                    $action = 'Detail';
                    $role = "data-eprole='form'";
                  }
                  ?>
                  <form data-reset='<?php echo $reset ?>' 
                      id='frmProduct' 
                      action='?c=product&a=action<?php echo $action ?>' 
                      method='POST' 
                      <?php echo $role ?>
                      ><div class='row'>
        <div class='col-sm-12'>
                <div class='form-group'>
                    <h2>Datos de Product</h2>
                </div>
            </div>
        </div><div class='row'><div class='col-sm-4'>
            <div class='form-group'>
                <label for='prdId'>prdId:</label><input readonly 
                    minlength='1' 
                    maxlength='100000' 
                    type='number' 
                    class='form-control' 
                    id='prdId' 
                    name='prdId' 
                     /></div>
            </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='prdName'>prdName:</label><input  
                    minlength='1' 
                    maxlength='30' 
                    type='input' 
                    class='form-control' 
                    id='prdName' 
                    name='prdName' 
                    required /></div>
            </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='prdDescription'>prdDescription:</label><textarea  
                    class='form-control' 
                    id='prdDescription' 
                    name='prdDescription' 
                    required ></textarea></div>
            </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='prdImagen'>prdImagen:</label><input  
                    minlength='1' 
                    maxlength='100' 
                    type='input' 
                    class='form-control' 
                    id='prdImagen' 
                    name='prdImagen' 
                    required /></div>
            </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='tpId'>tpId:</label><select required class='form-control'id='tpId'name='tpId'><option value=''>[SELECCIONE OPCION]</option><?php foreach ($type_products as $entity) {echo "<option value='{$entity->getId()}'>{$entity->describeStr()}</option>";} ?></select></div>
            </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='proId'>proId:</label><select required class='form-control'id='proId'name='proId'><option value=''>[SELECCIONE OPCION]</option><?php foreach ($providers as $entity) {echo "<option value='{$entity->getId()}'>{$entity->describeStr()}</option>";} ?></select></div>
            </div></div><div class='row'>
            <div class='col-sm-12'>
                <button type='submit' 
                        class='btn btn-success' 
                        data-form='frmProduct'>Guardar</button>
                <?php
                  if(@$create){
                  ?>
                  <button type='reset' 
                        class='btn btn-danger' 
                        >Limpiar</button>
                  <?php
                  }else{
                  ?>
                  <a href='?c=product&a=viewCreate' class='btn btn-info'>Nuevo registro</a>
                  <?php
                  }
                  ?>
                <a href='?c=product' class='btn btn-warning'>Ver todos los registros</a>
                                
            </div>
            <hr class='d-sm-none'>
        </div></form>