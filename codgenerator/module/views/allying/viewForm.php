
                  <?php
                  if(@$create){
                    $reset = true;
                    $action = 'Create';
                    $role = '';
                  }else{
                    $reset = false;
                    $action = 'Detail';
                    $role = "data-eprole='form'";
                  }
                  ?>
                  <form data-reset='<?php echo $reset ?>' 
                      id='frmAllying' 
                      action='?c=allying&a=action<?php echo $action ?>' 
                      method='POST' 
                      <?php echo $role ?>
                      ><div class='row'>
        <div class='col-sm-12'>
                <div class='form-group'>
                    <h2>Datos de Allying</h2>
                </div>
            </div>
        </div><div class='row'><div class='col-sm-4'>
            <div class='form-group'>
                <label for='allId'>allId:</label><input readonly 
                    minlength='1' 
                    maxlength='100000' 
                    type='number' 
                    class='form-control' 
                    id='allId' 
                    name='allId' 
                     /></div>
            </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='allName'>allName:</label><input  
                    minlength='1' 
                    maxlength='45' 
                    type='input' 
                    class='form-control' 
                    id='allName' 
                    name='allName' 
                    required /></div>
            </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='allDescription'>allDescription:</label><textarea  
                    class='form-control' 
                    id='allDescription' 
                    name='allDescription' 
                    required ></textarea></div>
            </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='allLogo'>allLogo:</label><input  
                    minlength='1' 
                    maxlength='50' 
                    type='input' 
                    class='form-control' 
                    id='allLogo' 
                    name='allLogo' 
                     /></div>
            </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='allContact'>allContact:</label><input  
                    minlength='1' 
                    maxlength='45' 
                    type='input' 
                    class='form-control' 
                    id='allContact' 
                    name='allContact' 
                     /></div>
            </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='allEmail'>allEmail:</label><input  
                    minlength='1' 
                    maxlength='45' 
                    type='input' 
                    class='form-control' 
                    id='allEmail' 
                    name='allEmail' 
                     /></div>
            </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='allAddress'>allAddress:</label><input  
                    minlength='1' 
                    maxlength='45' 
                    type='input' 
                    class='form-control' 
                    id='allAddress' 
                    name='allAddress' 
                     /></div>
            </div></div><div class='row'>
            <div class='col-sm-12'>
                <button type='submit' 
                        class='btn btn-success' 
                        data-form='frmAllying'>Guardar</button>
                <?php
                  if(@$create){
                  ?>
                  <button type='reset' 
                        class='btn btn-danger' 
                        >Limpiar</button>
                  <?php
                  }else{
                  ?>
                  <a href='?c=allying&a=viewCreate' class='btn btn-info'>Nuevo registro</a>
                  <?php
                  }
                  ?>
                <a href='?c=allying' class='btn btn-warning'>Ver todos los registros</a>
                                
            </div>
            <hr class='d-sm-none'>
        </div></form>