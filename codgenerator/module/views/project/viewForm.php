
                  <?php
                  if(@$create){
                    $reset = true;
                    $action = 'Create';
                    $role = '';
                  }else{
                    $reset = false;
                    $action = 'Detail';
                    $role = "data-eprole='form'";
                  }
                  ?>
                  <form data-reset='<?php echo $reset ?>' 
                      id='frmProject' 
                      action='?c=project&a=action<?php echo $action ?>' 
                      method='POST' 
                      <?php echo $role ?>
                      ><div class='row'>
        <div class='col-sm-12'>
                <div class='form-group'>
                    <h2>Datos de Project</h2>
                </div>
            </div>
        </div><div class='row'><div class='col-sm-4'>
            <div class='form-group'>
                <label for='pjtId'>pjtId:</label><input readonly 
                    minlength='1' 
                    maxlength='100000' 
                    type='number' 
                    class='form-control' 
                    id='pjtId' 
                    name='pjtId' 
                     /></div>
            </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='pjtName'>pjtName:</label><input  
                    minlength='1' 
                    maxlength='45' 
                    type='input' 
                    class='form-control' 
                    id='pjtName' 
                    name='pjtName' 
                    required /></div>
            </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='pjtDescription'>pjtDescription:</label><textarea  
                    class='form-control' 
                    id='pjtDescription' 
                    name='pjtDescription' 
                    required ></textarea></div>
            </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='pjtImagen'>pjtImagen:</label><input  
                    minlength='1' 
                    maxlength='45' 
                    type='input' 
                    class='form-control' 
                    id='pjtImagen' 
                    name='pjtImagen' 
                     /></div>
            </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='pjtFinalized'>pjtFinalized:</label><input  
                    minlength='1' 
                    maxlength='100000' 
                    type='number' 
                    class='form-control' 
                    id='pjtFinalized' 
                    name='pjtFinalized' 
                     /></div>
            </div></div><div class='row'>
            <div class='col-sm-12'>
                <button type='submit' 
                        class='btn btn-success' 
                        data-form='frmProject'>Guardar</button>
                <?php
                  if(@$create){
                  ?>
                  <button type='reset' 
                        class='btn btn-danger' 
                        >Limpiar</button>
                  <?php
                  }else{
                  ?>
                  <a href='?c=project&a=viewCreate' class='btn btn-info'>Nuevo registro</a>
                  <?php
                  }
                  ?>
                <a href='?c=project' class='btn btn-warning'>Ver todos los registros</a>
                                
            </div>
            <hr class='d-sm-none'>
        </div></form>