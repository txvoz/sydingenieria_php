
                  <?php
                  if(@$create){
                    $reset = true;
                    $action = 'Create';
                    $role = '';
                  }else{
                    $reset = false;
                    $action = 'Detail';
                    $role = "data-eprole='form'";
                  }
                  ?>
                  <form data-reset='<?php echo $reset ?>' 
                      id='frmContact' 
                      action='?c=contact&a=action<?php echo $action ?>' 
                      method='POST' 
                      <?php echo $role ?>
                      ><div class='row'>
        <div class='col-sm-12'>
                <div class='form-group'>
                    <h2>Datos de Contact</h2>
                </div>
            </div>
        </div><div class='row'><div class='col-sm-4'>
            <div class='form-group'>
                <label for='conId'>conId:</label><input readonly 
                    minlength='1' 
                    maxlength='100000' 
                    type='number' 
                    class='form-control' 
                    id='conId' 
                    name='conId' 
                     /></div>
            </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='conSubject'>conSubject:</label><input  
                    minlength='1' 
                    maxlength='45' 
                    type='input' 
                    class='form-control' 
                    id='conSubject' 
                    name='conSubject' 
                    required /></div>
            </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='conMessage'>conMessage:</label><textarea  
                    class='form-control' 
                    id='conMessage' 
                    name='conMessage' 
                    required ></textarea></div>
            </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='conName'>conName:</label><input  
                    minlength='1' 
                    maxlength='30' 
                    type='input' 
                    class='form-control' 
                    id='conName' 
                    name='conName' 
                    required /></div>
            </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='conEmail'>conEmail:</label><input  
                    minlength='1' 
                    maxlength='45' 
                    type='input' 
                    class='form-control' 
                    id='conEmail' 
                    name='conEmail' 
                    required /></div>
            </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='conPhone'>conPhone:</label><input  
                    minlength='1' 
                    maxlength='45' 
                    type='input' 
                    class='form-control' 
                    id='conPhone' 
                    name='conPhone' 
                     /></div>
            </div></div><div class='row'>
            <div class='col-sm-12'>
                <button type='submit' 
                        class='btn btn-success' 
                        data-form='frmContact'>Guardar</button>
                <?php
                  if(@$create){
                  ?>
                  <button type='reset' 
                        class='btn btn-danger' 
                        >Limpiar</button>
                  <?php
                  }else{
                  ?>
                  <a href='?c=contact&a=viewCreate' class='btn btn-info'>Nuevo registro</a>
                  <?php
                  }
                  ?>
                <a href='?c=contact' class='btn btn-warning'>Ver todos los registros</a>
                                
            </div>
            <hr class='d-sm-none'>
        </div></form>