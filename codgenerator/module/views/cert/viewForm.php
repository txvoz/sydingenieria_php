
                  <?php
                  if(@$create){
                    $reset = true;
                    $action = 'Create';
                    $role = '';
                  }else{
                    $reset = false;
                    $action = 'Detail';
                    $role = "data-eprole='form'";
                  }
                  ?>
                  <form data-reset='<?php echo $reset ?>' 
                      id='frmCert' 
                      action='?c=cert&a=action<?php echo $action ?>' 
                      method='POST' 
                      <?php echo $role ?>
                      ><div class='row'>
        <div class='col-sm-12'>
                <div class='form-group'>
                    <h2>Datos de Cert</h2>
                </div>
            </div>
        </div><div class='row'><div class='col-sm-4'>
            <div class='form-group'>
                <label for='cerId'>cerId:</label><input readonly 
                    minlength='1' 
                    maxlength='100000' 
                    type='number' 
                    class='form-control' 
                    id='cerId' 
                    name='cerId' 
                     /></div>
            </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='cerTitle'>cerTitle:</label><input  
                    minlength='1' 
                    maxlength='45' 
                    type='input' 
                    class='form-control' 
                    id='cerTitle' 
                    name='cerTitle' 
                    required /></div>
            </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='cerCode'>cerCode:</label><input  
                    minlength='1' 
                    maxlength='45' 
                    type='input' 
                    class='form-control' 
                    id='cerCode' 
                    name='cerCode' 
                    required /></div>
            </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='cerDescription'>cerDescription:</label><textarea  
                    class='form-control' 
                    id='cerDescription' 
                    name='cerDescription' 
                    required ></textarea></div>
            </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='cerFile'>cerFile:</label><input  
                    minlength='1' 
                    maxlength='50' 
                    type='input' 
                    class='form-control' 
                    id='cerFile' 
                    name='cerFile' 
                    required /></div>
            </div></div><div class='row'>
            <div class='col-sm-12'>
                <button type='submit' 
                        class='btn btn-success' 
                        data-form='frmCert'>Guardar</button>
                <?php
                  if(@$create){
                  ?>
                  <button type='reset' 
                        class='btn btn-danger' 
                        >Limpiar</button>
                  <?php
                  }else{
                  ?>
                  <a href='?c=cert&a=viewCreate' class='btn btn-info'>Nuevo registro</a>
                  <?php
                  }
                  ?>
                <a href='?c=cert' class='btn btn-warning'>Ver todos los registros</a>
                                
            </div>
            <hr class='d-sm-none'>
        </div></form>