<?php
class Gallery_itemController implements IController, IManagementForm, IAction  {
private $path = "rendered/gallery_item/";private $config = null;public function __construct() {
        $this->config = Config::singleton();
        require "{$this->config->get("entities")}Gallery_item.php";
        require "{$this->config->get("models")}Gallery_itemModel.php";
        }public function index() {
        $this->viewList();
        }public function viewCreate() {
        $vars = [];
        $vars["create"] = true;
        
        View::show("{$this->path}viewForm", $vars);
        }public function viewDetail() {
        $vars = [];
        $vars["id"] = $_REQUEST["acc"];
        
        View::show("{$this->path}viewForm", $vars);
        }public function viewList() {
        View::show("{$this->path}viewList");
        }public function actionList() {
        $arg = new stdClass();
        //*******************
        $m = new Gallery_itemModel();
        $arg->filtro = @$_REQUEST['search'];
        $arg->paginator = null;
        $rcount = $m->getCount($arg->filtro)->cantidad;
        $arg->paginator = new Paginator($rcount, @$_REQUEST['p']);
        $r = $m->get($arg, false);
        $m->lazyLoad($r->data);
        if ($arg->paginator !== null) {
            $r->paginator = $arg->paginator->renderPaginator();
        }
        $r->count = $rcount;
        echo json_encode($r);
        }public function actionListData() {
        //*******************
        $m = new Gallery_itemModel();
        $rcount = $m->getCount()->cantidad;
        $r = $m->get();
        $r->count = $rcount;
        echo json_encode($r);
        }public function actionDetail() {
            if ($_SERVER['REQUEST_METHOD'] === "GET") {
                $r = null;
                $m = new Gallery_itemModel();
                $e = new Gallery_item();
                $e->setGtmId($_REQUEST["acc"]);
                $r = $m->getById($e);
                echo json_encode($r);
            } else if ($_SERVER['REQUEST_METHOD'] === "POST") {
                $this->actionUpdate();
            }
        }public function actionCreate() {
        $data = Utils::getParamsByBody();
        $e = new Gallery_item();
        $e->serializeByObject($data);
        $m = new Gallery_itemModel();
        $r = $m->insert($e);
        echo json_encode($r);
        }public function actionDelete() {
        $m = new Gallery_itemModel();
        $e = new Gallery_item();
        $e->setGtmId($_REQUEST["acc"]);
        $r = $m->delete($e);
        echo json_encode($r);
        }public function actionUpdate() {
        $data = Utils::getParamsByBody();
        $e = new Gallery_item();
        $e->serializeByObject($data);
        $m = new Gallery_itemModel();
        $r = $m->update($e);
        echo json_encode($r);
        }
}