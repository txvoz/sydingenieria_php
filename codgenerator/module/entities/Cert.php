<?php
class Cert extends BasicEntity implements JsonSerializable, IEntity {
/* Attributes */
/* @PrimaryKey */
	protected $cerId;
	protected $cerTitle;
	protected $cerCode;
	protected $cerDescription;
	protected $cerFile;
/* Getters */
	public function getCerId(){
		return $this->cerId;
	}

	public function getCerTitle(){
		return $this->cerTitle;
	}

	public function getCerCode(){
		return $this->cerCode;
	}

	public function getCerDescription(){
		return $this->cerDescription;
	}

	public function getCerFile(){
		return $this->cerFile;
	}

	public function getId(){
		return $this->getCerId();
	}

/* Setters */
	public function setCerId($param){
$this->setId($param);
		$this->cerId = $param;
	}

	public function setCerTitle($param){
		$this->cerTitle = $param;
	}

	public function setCerCode($param){
		$this->cerCode = $param;
	}

	public function setCerDescription($param){
		$this->cerDescription = $param;
	}

	public function setCerFile($param){
		$this->cerFile = $param;
	}

public function jsonSerialize() {
        $this->id = $this->cerId;
        return get_object_vars($this);
        }
        
        public function lazyLoad() {
        }
        
        /*public function serializeByArray($array) {
            foreach ($array as $key => $value) {
                $this->{"{$key}"} = $value;
            }
        }

        public function serializeByObject($o) {
            foreach ($o as $key => $value) {
                $this->{"{$key}"} = $value;
            }
        }*/
}