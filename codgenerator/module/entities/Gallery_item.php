<?php
class Gallery_item extends BasicEntity implements JsonSerializable, IEntity {
/* Attributes */
/* @PrimaryKey */
	protected $gtmId;
	protected $gtmTitle;
	protected $gtmDescription;
	protected $gtmImage;
/* Getters */
	public function getGtmId(){
		return $this->gtmId;
	}

	public function getGtmTitle(){
		return $this->gtmTitle;
	}

	public function getGtmDescription(){
		return $this->gtmDescription;
	}

	public function getGtmImage(){
		return $this->gtmImage;
	}

	public function getId(){
		return $this->getGtmId();
	}

/* Setters */
	public function setGtmId($param){
$this->setId($param);
		$this->gtmId = $param;
	}

	public function setGtmTitle($param){
		$this->gtmTitle = $param;
	}

	public function setGtmDescription($param){
		$this->gtmDescription = $param;
	}

	public function setGtmImage($param){
		$this->gtmImage = $param;
	}

public function jsonSerialize() {
        $this->id = $this->gtmId;
        return get_object_vars($this);
        }
        
        public function lazyLoad() {
        }
        
        /*public function serializeByArray($array) {
            foreach ($array as $key => $value) {
                $this->{"{$key}"} = $value;
            }
        }

        public function serializeByObject($o) {
            foreach ($o as $key => $value) {
                $this->{"{$key}"} = $value;
            }
        }*/
}