<?php
class Provider extends BasicEntity implements JsonSerializable, IEntity {
/* Attributes */
/* @PrimaryKey */
	protected $proId;
	protected $proNit;
	protected $proName;
	protected $proDescription;
	protected $proLogo;
	protected $proWebSite;
/* Getters */
	public function getProId(){
		return $this->proId;
	}

	public function getProNit(){
		return $this->proNit;
	}

	public function getProName(){
		return $this->proName;
	}

	public function getProDescription(){
		return $this->proDescription;
	}

	public function getProLogo(){
		return $this->proLogo;
	}

	public function getProWebSite(){
		return $this->proWebSite;
	}

	public function getId(){
		return $this->getProId();
	}

/* Setters */
	public function setProId($param){
$this->setId($param);
		$this->proId = $param;
	}

	public function setProNit($param){
		$this->proNit = $param;
	}

	public function setProName($param){
		$this->proName = $param;
	}

	public function setProDescription($param){
		$this->proDescription = $param;
	}

	public function setProLogo($param){
		$this->proLogo = $param;
	}

	public function setProWebSite($param){
		$this->proWebSite = $param;
	}

public function jsonSerialize() {
        $this->id = $this->proId;
        return get_object_vars($this);
        }
        
        public function lazyLoad() {
        }
        
        /*public function serializeByArray($array) {
            foreach ($array as $key => $value) {
                $this->{"{$key}"} = $value;
            }
        }

        public function serializeByObject($o) {
            foreach ($o as $key => $value) {
                $this->{"{$key}"} = $value;
            }
        }*/
}