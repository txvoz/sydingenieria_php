<?php
class Allying extends BasicEntity implements JsonSerializable, IEntity {
/* Attributes */
/* @PrimaryKey */
	protected $allId;
	protected $allName;
	protected $allDescription;
	protected $allLogo;
	protected $allContact;
	protected $allEmail;
	protected $allAddress;
/* Getters */
	public function getAllId(){
		return $this->allId;
	}

	public function getAllName(){
		return $this->allName;
	}

	public function getAllDescription(){
		return $this->allDescription;
	}

	public function getAllLogo(){
		return $this->allLogo;
	}

	public function getAllContact(){
		return $this->allContact;
	}

	public function getAllEmail(){
		return $this->allEmail;
	}

	public function getAllAddress(){
		return $this->allAddress;
	}

	public function getId(){
		return $this->getAllId();
	}

/* Setters */
	public function setAllId($param){
$this->setId($param);
		$this->allId = $param;
	}

	public function setAllName($param){
		$this->allName = $param;
	}

	public function setAllDescription($param){
		$this->allDescription = $param;
	}

	public function setAllLogo($param){
		$this->allLogo = $param;
	}

	public function setAllContact($param){
		$this->allContact = $param;
	}

	public function setAllEmail($param){
		$this->allEmail = $param;
	}

	public function setAllAddress($param){
		$this->allAddress = $param;
	}

public function jsonSerialize() {
        $this->id = $this->allId;
        return get_object_vars($this);
        }
        
        public function lazyLoad() {
        }
        
        /*public function serializeByArray($array) {
            foreach ($array as $key => $value) {
                $this->{"{$key}"} = $value;
            }
        }

        public function serializeByObject($o) {
            foreach ($o as $key => $value) {
                $this->{"{$key}"} = $value;
            }
        }*/
}