<?php
class Type_product extends BasicEntity implements JsonSerializable, IEntity {
/* Attributes */
/* @PrimaryKey */
	protected $tpId;
	protected $tpName;
/* Getters */
	public function getTpId(){
		return $this->tpId;
	}

	public function getTpName(){
		return $this->tpName;
	}

	public function getId(){
		return $this->getTpId();
	}

/* Setters */
	public function setTpId($param){
$this->setId($param);
		$this->tpId = $param;
	}

	public function setTpName($param){
		$this->tpName = $param;
	}

public function jsonSerialize() {
        $this->id = $this->tpId;
        return get_object_vars($this);
        }
        
        public function lazyLoad() {
        }
        
        /*public function serializeByArray($array) {
            foreach ($array as $key => $value) {
                $this->{"{$key}"} = $value;
            }
        }

        public function serializeByObject($o) {
            foreach ($o as $key => $value) {
                $this->{"{$key}"} = $value;
            }
        }*/
}