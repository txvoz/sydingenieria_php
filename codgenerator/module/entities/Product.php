<?php
class Product extends BasicEntity implements JsonSerializable, IEntity {
/* Attributes */
/* @PrimaryKey */
	protected $prdId;
	protected $prdName;
	protected $prdDescription;
	protected $prdImagen;
/* @Index */
	protected $fktype_product = null;
	protected $tpId;
/* @Index */
	protected $fkprovider = null;
	protected $proId;
/* Getters */
	public function getPrdId(){
		return $this->prdId;
	}

	public function getPrdName(){
		return $this->prdName;
	}

	public function getPrdDescription(){
		return $this->prdDescription;
	}

	public function getPrdImagen(){
		return $this->prdImagen;
	}

	public function getTpId(){
		return $this->tpId;
	}

/** Index **/
	public function getFkType_product(){
if($this->fktype_product===null){$model = new type_productModel();$e = new Type_product();$e->setTpId($this->tpId);$r = $model->getById($e);if($r->status===200){$this->fktype_product = $model->getById($e)->data;}}		return $this->fktype_product;
	}

	public function getProId(){
		return $this->proId;
	}

/** Index **/
	public function getFkProvider(){
if($this->fkprovider===null){$model = new providerModel();$e = new Provider();$e->setProId($this->proId);$r = $model->getById($e);if($r->status===200){$this->fkprovider = $model->getById($e)->data;}}		return $this->fkprovider;
	}

	public function getId(){
		return $this->getPrdId();
	}

/* Setters */
	public function setPrdId($param){
$this->setId($param);
		$this->prdId = $param;
	}

	public function setPrdName($param){
		$this->prdName = $param;
	}

	public function setPrdDescription($param){
		$this->prdDescription = $param;
	}

	public function setPrdImagen($param){
		$this->prdImagen = $param;
	}

	public function setTpId($param){
		$this->tpId = $param;
	}

	public function setProId($param){
		$this->proId = $param;
	}

public function jsonSerialize() {
        $this->id = $this->prdId;
        return get_object_vars($this);
        }
        
        public function lazyLoad() {
        $this->getFkType_product();$this->getFkProvider();}
        
        /*public function serializeByArray($array) {
            foreach ($array as $key => $value) {
                $this->{"{$key}"} = $value;
            }
        }

        public function serializeByObject($o) {
            foreach ($o as $key => $value) {
                $this->{"{$key}"} = $value;
            }
        }*/
}