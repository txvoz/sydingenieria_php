<?php

class Precode {

    private $conexion;
    private $nameEntity = "stdClass";

    public function __construct() {
        $this->conexion = MyPDO::singleton();
    }

    public function get($obj = null) {
        $retorno = new stdClass();
        try {
            $sql = "show tables";
            $query = $this->conexion->prepare($sql);
            $query->execute();
            $retorno->data = $query->fetchAll(PDO::FETCH_CLASS, $this->nameEntity);
            $retorno->status = 200;
            $retorno->msg = "Consulta exitosa";
            if (count($retorno->data) === 0) {
                $retorno->status = 201;
                $retorno->msg = "No hay registros en la base de datos.";
            }
        } catch (PDOException $e) {
            $retorno->msg = $e->getMessage();
            $retorno->status = 501;
        }
        return $retorno;
    }

    public function getAttributesFromTable($table) {
        $retorno = new stdClass();
        try {
            $sql = "SHOW COLUMNS FROM `{$table}`";
            $query = $this->conexion->prepare($sql);
            $query->execute();
            $retorno->data = $query->fetchAll(PDO::FETCH_CLASS, $this->nameEntity);
            $retorno->status = 200;
            $retorno->msg = "Consulta exitosa";
            //print_r($retorno);
            //die();
            if (count($retorno->data) === 0) {
                $retorno->status = 201;
                $retorno->msg = "No hay registros en la base de datos.";
            }
        } catch (PDOException $e) {
            $retorno->msg = $e->getMessage();
            $retorno->status = 501;
        }
        return $retorno;
    }

    public function getConstrains($table) {
        $conf = Config::singleton();
        $schema = $conf->get("dbname");
        $retorno = new stdClass();
        try {
            $sql = "select DISTINCT(COLUMN_NAME) as columna, 
                CONSTRAINT_NAME as constraint_ref, 
                REFERENCED_COLUMN_NAME as columna_referencia, 
                REFERENCED_TABLE_NAME as tabla_referencia
            from information_schema.KEY_COLUMN_USAGE
            where REFERENCED_COLUMN_NAME is not null 
               and TABLE_NAME = '{$table}' and TABLE_SCHEMA = '{$schema}'";
            $query = $this->conexion->prepare($sql);
            $query->execute();
            $retorno->data = $query->fetchAll(PDO::FETCH_CLASS, $this->nameEntity);
            $retorno->status = 200;
            $retorno->msg = "Consulta exitosa";
            //print_r($retorno);
            //die();
            if (count($retorno->data) === 0) {
                $retorno->status = 201;
                $retorno->msg = "No hay registros en la base de datos.";
            }
        } catch (PDOException $e) {
            $retorno->msg = $e->getMessage();
            $retorno->status = 501;
        }
        return $retorno;
    }

    public function getConstrainsX($table) {
        $retorno = new stdClass();
        try {
            $sql = "SELECT * FROM information_schema.TABLE_CONSTRAINTS 
            WHERE information_schema.TABLE_CONSTRAINTS.CONSTRAINT_TYPE = 'FOREIGN KEY' 
            AND information_schema.TABLE_CONSTRAINTS.TABLE_SCHEMA = '{$schema}'
            AND information_schema.TABLE_CONSTRAINTS.TABLE_NAME = '{$table}'";
            $query = $this->conexion->prepare($sql);
            $query->execute();
            $retorno->data = $query->fetchAll(PDO::FETCH_CLASS, $this->nameEntity);
            $retorno->status = 200;
            $retorno->msg = "Consulta exitosa";
            //print_r($retorno);
            //die();
            if (count($retorno->data) === 0) {
                $retorno->status = 201;
                $retorno->msg = "No hay registros en la base de datos.";
            }
        } catch (PDOException $e) {
            $retorno->msg = $e->getMessage();
            $retorno->status = 501;
        }
        return $retorno;
    }

}
