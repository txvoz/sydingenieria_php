<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CodeGenerator
 *
 * @author USER
 */
class CodeGenerator {

    private static $instance = null;
    private $resultDB = null;
    private $referenceTable = "";
    private $precode = null;
    private $path = "";
    private $pathViews = "";
    private $tabla = "";
    private $atributos = null;
    private $primary = "";
    private $primaryKeys = [];

    public static function singleton() {
        if (self::$instance === null) {
            $nombreClase = __CLASS__;
            self::$instance = new $nombreClase();
        }
        return self::$instance;
    }

    private function __construct() {
        $this->precode = new Precode();
        $this->resultDB = $this->precode->get();
        if (count($this->resultDB->data) > 0) {
            foreach ($this->resultDB->data[0] as $key => $value) {
                $this->referenceTable = $key;
                break;
            }
        }
    }

    public function generateAll() {
        $this->getPrimaryKeys();
        foreach ($this->resultDB->data as $table) {
            $this->tabla = $table->{$this->referenceTable};
            $this->atributos = $this->precode->getAttributesFromTable($this->tabla)->data;
            $this->pathViews = "module/views/{$this->tabla}/";
            @mkdir($this->pathViews, 777);
            echo "<pre>";
            echo "{$this->tabla}<br>----------------------<br>";
            $this->path = "module/";
            $this->primary = $this->getPrimaryKey($this->atributos);
            $this->generateEntity();
            $this->generateModel();
            $this->generateController();
            $this->generateViewList();
            $this->generateViewForm();
            echo "</pre>";
        }
        $this->createMenu();
    }

    public function getPrimaryKey($atributos) {
        $primary = "";
        foreach ($atributos as $atributo) {
            if ($atributo->Key === "PRI") {
                $primary = $atributo->Field;
                break;
            }
        }
        return $primary;
    }

    public function getPrimaryReferences() {
        $data = $this->precode->getConstrains($this->tabla)->data;
        $references = [];
        foreach ($data as $reg) {
            $references[] = $reg->columna;
        }
//        print_r($references);
//        die();
        return $references;
    }

    //Falta completar
    public function getIndexes($atributos) {
        $references = [];
        foreach ($atributos as $atributo) {
            if ($atributo->Key === "MUL") {
                $references[] = $atributo->Field;
            }
        }
        return $references;
    }

    private function getPrimaryKeys() {
        foreach ($this->resultDB->data as $table) {
            $this->tabla = $table->{$this->referenceTable};
            $this->atributos = $this->precode->getAttributesFromTable($this->tabla)->data;
            $this->primary = $this->getPrimaryKey($this->atributos);
            $references = $this->getPrimaryReferences();
            $this->primaryKeys["{$this->primary}"] = [
                "table" => $this->tabla,
                "primary" => $this->primary,
                "references" => $references
            ];
        }
        echo "<pre>";
        print_r($this->primaryKeys);
        echo "</pre>";
    }

    private function generateController() {
        $nameCls = ucfirst($this->tabla);
//Controller
        $class = "<?php\nclass {$nameCls}Controller implements IController, IManagementForm, IAction  {\n";
        //Atributos
        $class .= "private \$path = \"rendered/{$this->tabla}/\";";
        $class .= "private \$config = null;";
        //Constructor
        $class .= "public function __construct() {
        \$this->config = Config::singleton();
        require \"{\$this->config->get(\"entities\")}{$nameCls}.php\";
        require \"{\$this->config->get(\"models\")}{$nameCls}Model.php\";
        ";

        $data = $this->primaryKeys["{$this->primary}"];
        foreach ($data["references"] as $key => $value) {
            $refTable = $this->primaryKeys["{$value}"];
            $table = ucfirst($refTable["table"]);
            $class .= "require \"{\$this->config->get(\"entities\")}{$table}.php\";"
                    . "require \"{\$this->config->get(\"models\")}{$table}Model.php\";";
        }

        $class .= "}";

        $class .= "public function index() {
        \$this->viewList();
        }";

        $class .= "public function viewCreate() {
        \$vars = [];
        \$vars[\"create\"] = true;
        ";

        $cont = 1;
        $data = $this->primaryKeys["{$this->primary}"];
        foreach ($data["references"] as $key => $value) {
            $refTable = $this->primaryKeys["{$value}"];
            $table = ucfirst($refTable["table"]);
            //************************
            $class .= "\$model{$cont} = new {$table}Model();";
            $class .= "\$vars['{$refTable["table"]}s'] = \$model{$cont}->get()->data;";
            //************************
            $cont++;
        }

        $class .= "
        View::show(\"{\$this->path}viewForm\", \$vars);
        }";

        $class .= "public function viewDetail() {
        \$vars = [];
        \$vars[\"id\"] = \$_REQUEST[\"acc\"];
        ";

        $cont = 1;
        $data = $this->primaryKeys["{$this->primary}"];
        foreach ($data["references"] as $key => $value) {
            $refTable = $this->primaryKeys["{$value}"];
            $table = ucfirst($refTable["table"]);
            //************************
            $class .= "\$model{$cont} = new {$table}Model();";
            $class .= "\$vars['{$refTable["table"]}s'] = \$model{$cont}->get()->data;";
            //************************
            $cont++;
        }

        $class .= "
        View::show(\"{\$this->path}viewForm\", \$vars);
        }";

        $class .= "public function viewList() {
        View::show(\"{\$this->path}viewList\");
        }";

        $class .= "public function actionList() {
        \$arg = new stdClass();
        //*******************
        \$m = new {$nameCls}Model();
        \$arg->filtro = @\$_REQUEST['search'];
        \$arg->paginator = null;
        \$rcount = \$m->getCount(\$arg->filtro)->cantidad;
        \$arg->paginator = new Paginator(\$rcount, @\$_REQUEST['p']);
        \$r = \$m->get(\$arg, false);
        \$m->lazyLoad(\$r->data);
        if (\$arg->paginator !== null) {
            \$r->paginator = \$arg->paginator->renderPaginator();
        }
        \$r->count = \$rcount;
        echo json_encode(\$r);
        }";

        $class .= "public function actionListData() {
        //*******************
        \$m = new {$nameCls}Model();
        \$rcount = \$m->getCount()->cantidad;
        \$r = \$m->get();
        \$r->count = \$rcount;
        echo json_encode(\$r);
        }";

        $primaryUP = ucfirst($this->primary);
        $class .= "public function actionDetail() {
            if (\$_SERVER['REQUEST_METHOD'] === \"GET\") {
                \$r = null;
                \$m = new {$nameCls}Model();
                \$e = new {$nameCls}();
                \$e->set{$primaryUP}(\$_REQUEST[\"acc\"]);
                \$r = \$m->getById(\$e);
                echo json_encode(\$r);
            } else if (\$_SERVER['REQUEST_METHOD'] === \"POST\") {
                \$this->actionUpdate();
            }
        }";

        $class .= "public function actionCreate() {
        \$data = Utils::getParamsByBody();
        \$e = new {$nameCls}();
        \$e->serializeByObject(\$data);
        \$m = new {$nameCls}Model();
        \$r = \$m->insert(\$e);
        echo json_encode(\$r);
        }";

        $class .= "public function actionDelete() {
        \$m = new {$nameCls}Model();
        \$e = new {$nameCls}();
        \$e->set{$primaryUP}(\$_REQUEST[\"acc\"]);
        \$r = \$m->delete(\$e);
        echo json_encode(\$r);
        }";

        $class .= "public function actionUpdate() {
        \$data = Utils::getParamsByBody();
        \$e = new {$nameCls}();
        \$e->serializeByObject(\$data);
        \$m = new {$nameCls}Model();
        \$r = \$m->update(\$e);
        echo json_encode(\$r);
        }";

        $class .= "\n}";

        $file = fopen("{$this->path}controllers/{$nameCls}Controller.php", "w+");
        fwrite($file, $class);
        fclose($file);
        @chmod($file, 0777);
    }

    private function generateModel() {
        $nameCls = ucfirst($this->tabla);
//Model
        $class = "<?php\nclass {$nameCls}Model extends BasicModel implements IModel {\n";
//Atributos
        $class .= "//***** Attributes *****//\n";
        $class .= "\tprivate \$conexion;\n";
        @$class .= "\tprivate \$table = '{$this->tabla}';\n";
        $class .= "\tprivate \$nameEntity = '{$nameCls}';\n";
        $class .= "\tprivate \$filter = '{$this->atributos[1]->Field}';\n";
//Contructs
        $class .= "//***** Constructs *****//\n";
        $class .= "\tpublic function __construct(){\n";
        $class .= "\t\t \$this->conexion = MyPDO::singleton();\n";
        $class .= "\t}\n\n";
//Methods
        $class .= "//***** Methods *****//\n";

        $class .= "public function getCount(\$obj = null) {
    \t\$retorno = new stdClass();
    try {
      //****** Paginacion y Filtros
             \$where = '';
            if (\$obj != '') {
                \$where = \" where {\$this->filter} like '%{\$obj}%' \";
            }
            //*****
      \t\t\$sql = \"SELECT count(*) as cantidad FROM {\$this->table} {\$where} \";
      \t\t\$query = \$this->conexion->prepare(\$sql);
      \t\t\$query->execute();
      \t\t\$r = \$query->fetchObject();
      \t\t\$retorno->cantidad = \$r->cantidad;
    } catch (PDOException \$e) {
      \t\t\$retorno->status = 501;
      \t\t\$retorno->message = Utils::simpleCatchMessage(\$e->getMessage());
    }
    \treturn \$retorno;
  }";

        $class .= "\tpublic function get(\$obj=null,\$all=true){\n";
        $class .= "\t\$retorno = new stdClass()\n;
    \ttry {\n

    //****** Paginacion y Filtros
            \$where = '';
            \$limit = '';
            if (\$obj !== null) {
                if (\$obj->filtro != '') {
                    \$where = \" where {\$this->filter} like '%{\$obj->filtro}%' \";
                }
                if (!\$all) {
                    \$paginacion = \$obj->paginator;
                    \$paginacion instanceof Paginator;
                    \$c = Paginator::\$COUNT_BY_PAG;
                    \$i = \$paginacion->getInicioLimit();
                    \$limit = \"limit {\$i},{\$c}\";
                }
            }
            //*****

      //*****
      \$sql = \"select * from {\$this->table} {\$where} order by {\$this->filter} asc {\$limit}\";

      \t\t\$query = \$this->conexion->prepare(\$sql);
      \t\t\$query->execute();
      \t\t\$retorno->data = \$query->fetchAll(PDO::FETCH_CLASS, \$this->nameEntity);
      \t\t\$retorno->status = 200;
      \t\t\$retorno->message = \"Consulta exitosa\";
      \t\tif (count(\$retorno->data) === 0) {
        \t\t\$retorno->status = 201;
        \t\t\$retorno->message = \"No hay registros en la base de datos.\";
      \t\t}
    \t} catch (PDOException \$e) {
      \t\t\$retorno->message = Utils::simpleCatchMessage(\$e->getMessage());
      \t\t\$retorno->status = 501;
    \t}
    //\$this->lazyLoad(\$retorno->data);
    \treturn \$retorno;\n";

        $class .= "\t}\n\n";

        $namePRI = ucfirst($this->primary);
        $class .= "\tpublic function getById(\$entity){\n";
        @$class .= "\$retorno = new stdClass();
    try {
      \$entity instanceof {$nameCls};
      \$sql = \"select * from {\$this->table} where {$this->primary} = ?\";
      \$query = \$this->conexion->prepare(\$sql);
      @\$query->bindParam(1, \$entity->get{$namePRI}());
      \$query->execute();
      \$retorno->data = \$query->fetchObject(\$this->nameEntity);
      \$retorno->status = 200;
      \$retorno->message = \"{\$this->nameEntity} \encontrado\";
      if (!\$retorno->data instanceof \$this->nameEntity) {
        \$retorno->status = 201;
        \$retorno->message = \"{\$this->nameEntity} no encontrado\";
      }
    } catch (PDOException \$e) {
      \$retorno->message = Utils::simpleCatchMessage(\$e->getMessage());
      \$retorno->status = 501;
    }
    //\$this->lazyLoad(\$retorno->data);
    return \$retorno;\n";
        $class .= "\t}\n\n";

        $class .= "\tpublic function insert(\$entity){\n";
        $class .= "\$retorno = new stdClass();
    try {
      \$entity instanceof {$nameCls}; \n";

        $countAtt = count($this->atributos);

        $class .= "\$sql = \"insert into {\$this->table} values ( null,";
        $i = 1;
        foreach ($this->atributos as $obj2) {
            if ($obj2->Field !== $this->primary) {
                $class .= "?";
                if ($i < ($countAtt - 1)) {
                    $class .= ",";
                }
                $i++;
            }
        }
        $class .= ")\";\n";
        $class .= "\$query = \$this->conexion->prepare(\$sql);";
        $i = 1;
        foreach ($this->atributos as $obj2) {
            if ($obj2->Field !== $this->primary) {
                $name = ucfirst($obj2->Field);
                $class .= "\t@\$query->bindParam({$i}, \$entity->get{$name}());";
                $i++;
            }
        }
        @$class .= "
      \$query->execute();
      \$id = \$this->conexion->lastInsertId();
      \$entity->set{$namePRI}(\$id);
      \$retorno->data = \$entity;
      \$retorno->status = 200;
      \$retorno->message = \"Registro insertado.\";
    } catch (PDOException \$e) {
      \$retorno->message = Utils::simpleCatchMessage(\$e->getMessage());
      \$retorno->status = 501;
    }
    return \$retorno;\n";
        $class .= "\t}\n\n";

        $class .= "\tpublic function update(\$entity){\n";
        $class .= "\$retorno = new stdClass();
    try {
      \$entity instanceof {$nameCls};";

        $class .= "
      \$sql = \"update {\$this->table} set \" \n";

        $i = 1;
        foreach ($this->atributos as $obj2) {
            if ($obj2->Field !== $this->primary) {
                $class .= ".\"{$obj2->Field} = ?";
                if ($i < ($countAtt - 1)) {
                    $class .= ",\"\n";
                } else {
                    $class .= " \"\n";
                }
                $i++;
            }
        }

        @$class .= ". \"where {$this->primary}=?\"; ";

        $class .= "
      \$query = \$this->conexion->prepare(\$sql); ";

        $i = 1;
        foreach ($this->atributos as $obj2) {
            if ($obj2->Field !== $this->primary) {
                $name = ucfirst($obj2->Field);
                $class .= "\t@\$query->bindParam({$i}, \$entity->get{$name}());";
                $i++;
            }
        }

        $class .= "\t@\$query->bindParam({$i}, \$entity->get{$namePRI}());";

        $class .= "
      \$query->execute();
      \$retorno->data = \$entity;
      \$retorno->status = 200;
      \$retorno->message = \"Registro Actualizado.\";
    } catch (PDOException \$e) {
      \$retorno->message = Utils::simpleCatchMessage(\$e->getMessage());
      \$retorno->status = 501;
    }
    return \$retorno;";
        $class .= "\t}\n\n";

        $class .= "\tpublic function delete(\$entity){\n";
        @$class .= "\$retorno = new stdClass();
    try {
      \$entity instanceof {$nameCls};
      \$sql = \"delete from {\$this->table} where {$this->primary} = ?\";
      \$query = \$this->conexion->prepare(\$sql);
      @\$query->bindParam(1, \$entity->get{$namePRI}());
      \$query->execute();
      \$retorno->status = 200;
      \$retorno->message = \"{\$this->nameEntity} eliminado\";
    } catch (PDOException \$e) {
      \$retorno->message = Utils::simpleCatchMessage(\$e->getMessage());
      \$retorno->status = 501;
    }
    return \$retorno;";
        $class .= "\t}\n\n";

        $class .= "\n}";

        $file = fopen("{$this->path}models/{$nameCls}Model.php", "w+");
        fwrite($file, $class);
        fclose($file);
        @chmod($file, 0777);
    }

    private function generateEntity() {
        //Entity
        $nameCls = ucfirst($this->tabla);
        $class = "<?php\nclass {$nameCls} extends BasicEntity implements JsonSerializable, IEntity {\n";

//Atributos
        $class .= "/* Attributes */\n";
        foreach ($this->atributos as $obj2) {
            print_r($obj2);
            if ($obj2->Key === "PRI") {
                $primary = $obj2->Field;
                $GLOBALS["primary"] = $primary;
                $class .= "/* @PrimaryKey */\n";
            } else if ($obj2->Key === "MUL") {
                $class .= "/* @Index */\n";
                $dataTable = $this->primaryKeys["{$obj2->Field}"];
                $table = $dataTable["table"];
                $class .= "\tprotected \$fk{$table} = null;\n";
            }
            $class .= "\tprotected \${$obj2->Field};\n";
        }

//Getter
        $class .= "/* Getters */\n";
        foreach ($this->atributos as $obj2) {
            $name = ucfirst($obj2->Field);
            $class .= "\tpublic function get{$name}(){\n";
            $class .= "\t\treturn \$this->{$obj2->Field};\n";
            $class .= "\t}\n\n";

            if ($obj2->Key === "MUL") {
                $dataTable = $this->primaryKeys["{$obj2->Field}"];
                $table = $dataTable["table"];
                $tableM = ucfirst($table);
                $pryM = ucfirst($obj2->Field);
                $class .= "/** Index **/";
                $class .= "\n\tpublic function getFk{$tableM}(){\n";
                $class .= "if(\$this->fk{$table}===null){"
                        . "\$model = new {$table}Model();"
                        . "\$e = new {$tableM}();"
                        . "\$e->set{$pryM}(\$this->{$obj2->Field});"
                        . "\$r = \$model->getById(\$e);"
                        . "if(\$r->status===200){"
                        . "\$this->fk{$table} = \$model->getById(\$e)->data;"
                        . "}"
                        . "}";
                $class .= "\t\treturn \$this->fk{$table};\n";
                $class .= "\t}\n\n";
            }
        }


        $name = ucfirst($this->primary);
        $class .= "\tpublic function getId(){\n";
        $class .= "\t\treturn \$this->get{$name}();\n";
        $class .= "\t}\n\n";

//Setter
        $class .= "/* Setters */\n";
        foreach ($this->atributos as $obj2) {
            $name = ucfirst($obj2->Field);
            $class .= "\tpublic function set{$name}(\$param){\n";
            if ($obj2->Key === "PRI") {
                $class .= "\$this->setId(\$param);\n";
            }
            $class .= "\t\t\$this->{$obj2->Field} = \$param;\n";
            $class .= "\t}\n\n";
        }

        $class .= "public function jsonSerialize() {
        \$this->id = \$this->{$this->primary};
        return get_object_vars(\$this);
        }
        
        public function lazyLoad() {
        ";
        foreach ($this->atributos as $obj2) {
            if ($obj2->Key === "MUL") {
                $dataTable = $this->primaryKeys["{$obj2->Field}"];
                $table = $dataTable["table"];
                $tableM = ucfirst($table);
                $class .= "\$this->getFk{$tableM}();";
            }
        }
        $class .= "}
        
        /*public function serializeByArray(\$array) {
            foreach (\$array as \$key => \$value) {
                \$this->{\"{\$key}\"} = \$value;
            }
        }

        public function serializeByObject(\$o) {
            foreach (\$o as \$key => \$value) {
                \$this->{\"{\$key}\"} = \$value;
            }
        }*/";

        $class .= "\n}";

        $file = fopen("{$this->path}entities/{$nameCls}.php", "w+");
        fwrite($file, $class);
        fclose($file);
        @chmod($file, 0777);
    }

    private function generateViewList() {
        $ref_table = $this->referenceTable;
        $primary = $this->primary;
        $nameCls = ucfirst($this->tabla);
        $nameMin = strtolower($this->tabla);
        //******
        $html = "<div class='row'>"
                . "<div class='col-sm-12'>"
                . "<h2>Lista de {$nameCls}s</h2>"
                . "<br>"
                . "<div data-eprole='list'data-paginator='true' data-endpoint='?c={$this->tabla}&a=actionList' data-cb='printDataSource'>"
                . "<form  action='#' data-eprole='filter'>
                     <div class='row'>
                         <div class='col-3'>
                             <input placeholder='Buscar...' class='form-control' type='search' id='search' name='search' value='' />
                         </div>
                         <div class='col-2'>
                             <button class='btn btn-danger'>Buscar</button>
                         </div>
                     </div>
                 </form>"
                . "<br>"
                . "<table class='table table-striped'>"
                . "<thead class='bg-primary navbar-dark text-white'>"
                . "<tr>";
        foreach ($this->atributos as $obj2) {
            $minAttr = strtolower($obj2->Field);
            $html .= "<th data-property='{$obj2->Field}'>{$obj2->Field}</th>\n";
        }
        $html .= "<th data-btns='delete,edit,*detail'>Opciones</th>";
        $html .= "</tr>"
                . "</thead>"
                . "<tbody class='list'></tbody>"
                . "</table>"
                . "<nav></nav>"
                . "</div><a href='?c={$nameMin}&a=viewCreate' class='btn btn-info'>Nuevo registro</a>"
                . "</div>"
                . "</div>";


        $file = fopen("{$this->pathViews}/viewList.php", "w+");
        fwrite($file, $html);
        fclose($file);
        @chmod($file, 0777);
    }

    private function generateViewForm() {
        $ref_table = $this->referenceTable;
        $primary = $this->primary;
        $nameCls = ucfirst($this->tabla);
        $nameMin = strtolower($this->tabla);
        $html = "
                  <?php
                  if(@\$create){
                    \$reset = true;
                    \$action = 'Create';
                    \$role = '';
                  }else{
                    \$reset = false;
                    \$action = 'Detail';
                    \$role = \"data-eprole='form'\";
                  }
                  ?>
                  <form data-reset='<?php echo \$reset ?>' 
                      id='frm{$nameCls}' 
                      action='?c={$nameMin}&a=action<?php echo \$action ?>' 
                      method='POST' 
                      <?php echo \$role ?>
                      >"
                . "<div class='row'>
        <div class='col-sm-12'>
                <div class='form-group'>
                    <h2>Datos de {$nameCls}</h2>
                </div>
            </div>
        </div>";

        $html .= "<div class='row'>";

        foreach ($this->atributos as $obj2) {
            $minAttr = strtolower($obj2->Field);
            /*             * * */
            $required = "";
            if ($obj2->Null === "NO") {
                $required = "required";
            }
            $readonly = "";
            if ($obj2->Key === "PRI") {
                $required = "";
                $readonly = "readonly";
            }

            $type = "";
            $max = "";
            $data = [];
            if ($obj2->Type === "date") {
                $type = "date";
            } else if ($obj2->Type === "text") {
                $type = "text";
            } else if (strpos($obj2->Type, "enum") !== false) {
                $type = "select";
                $data = str_replace("enum(", "", $obj2->Type);
                $data = str_replace(")", "", $data);
                $data = explode(",", $data);
            } else if (strpos($obj2->Type, "int") === false) {
                $type = "input";
                $max = str_replace("varchar(", "", $obj2->Type);
                $max = str_replace(")", "", $max);
            } else {
                $type = "number";
                $max = "100000";
            }

            $html .= "<div class='col-sm-4'>
            <div class='form-group'>
                <label for='{$obj2->Field}'>{$obj2->Field}:</label>";

            if ($obj2->Key !== "MUL") {
                if ($type === "text") {
                    $html .= "<textarea {$readonly} 
                    class='form-control' 
                    id='{$obj2->Field}' 
                    name='{$obj2->Field}' 
                    {$required} ></textarea>";
                } else if ($type !== "select") {
                    $html .= "<input {$readonly} 
                    minlength='1' 
                    maxlength='{$max}' 
                    type='{$type}' 
                    class='form-control' 
                    id='{$obj2->Field}' 
                    name='{$obj2->Field}' 
                    {$required} />";
                } else {
                    $html .= "<select required class='form-control'"
                            . "id='{$obj2->Field}'"
                            . "name='{$obj2->Field}'>"
                            . "<option value=''>[SELECCIONE OPCION]</option>";
                    foreach ($data as $value) {
                        $value = str_replace("'", "", $value);
                        $value = str_replace('"', "", $value);
                        $html .= "<option value='{$value}'>{$value}</option>";
                    }
                    $html .= "</select>";
                }
            } else {
                $html .= "<select required class='form-control'"
                        . "id='{$obj2->Field}'"
                        . "name='{$obj2->Field}'>"
                        . "<option value=''>[SELECCIONE OPCION]</option>"
                        . "<?php foreach (\${$this->primaryKeys[$obj2->Field]['table']}s as \$entity) {"
                        . "echo \"<option value='{\$entity->getId()}'>{\$entity->describeStr()}</option>\";"
                        . "} ?>";
                $html .= "</select>";
            }

            $html .= "</div>
            </div>";
        }

        $html .= "</div>";

        $html .= "<div class='row'>
            <div class='col-sm-12'>
                <button type='submit' 
                        class='btn btn-success' 
                        data-form='frm{$nameCls}'>Guardar</button>
                <?php
                  if(@\$create){
                  ?>
                  <button type='reset' 
                        class='btn btn-danger' 
                        >Limpiar</button>
                  <?php
                  }else{
                  ?>
                  <a href='?c={$nameMin}&a=viewCreate' class='btn btn-info'>Nuevo registro</a>
                  <?php
                  }
                  ?>
                <a href='?c={$nameMin}' class='btn btn-warning'>Ver todos los registros</a>
                                
            </div>
            <hr class='d-sm-none'>
        </div>"
                . "</form>";

        $file = fopen("{$this->pathViews}/viewForm.php", "w+");
        fwrite($file, $html);
        fclose($file);
        @chmod($file, 0777);
    }

    public function createMenu() {
        $html = "<ul class='nav nav-pills'>";
        foreach ($this->resultDB->data as $table) {
            $this->tabla = $table->{$this->referenceTable};
            $html .= "
<li class='nav-item dropdown'>
    <a class='nav-link dropdown-toggle text-white' data-toggle='dropdown' href='#'>{$this->tabla}</a>
    <div class='dropdown-menu'>
        <a class='dropdown-item' href='?c={$this->tabla}&a=viewCreate'>Nuevo registro</a>
        <a class='dropdown-item' href='?c={$this->tabla}'>Listar registros</a>
    </div>
</li>                
      ";
        }
        $html .= '</ul>';
        $file = fopen("menu.php", "w+");
        fwrite($file, $html);
        fclose($file);
        @chmod($file, 0777);
    }

    public function getContrains() {
        
    }

}
