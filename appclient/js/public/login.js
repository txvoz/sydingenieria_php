//Metodo para validar el metodo de login
function successLogin(r) {
    localStorage.setItem("login",$("#login").val());
    localStorage.setItem("password",$("#password").val());
    m = new Object();
    m.status = 200;
    m.message = "En un instante será redireccionado...!";
    createMessage(m);
    window.setTimeout(function(){
        window.location.replace("index.php");
    },3000);
    
}

//Metodo para recordar credenciales checkbox
$(function () {
    var est_old = localStorage.getItem("recordar");
    if(est_old){
        $("#recordar").attr("checked",est_old);
        var usuCorreo = localStorage.getItem("usuCorreo");
        $("#usuCorreo").val(usuCorreo);
        var usuContrasenia = localStorage.getItem("usuContrasenia");
        $("#usuContrasenia").val(usuContrasenia);
        if(usuCorreo && usuContrasenia){
            $("#frmLogin").submit();
        }
    }
    $("#recordar").change(function(){
        var est = $(this).is(":checked");
        if(est){
            localStorage.setItem("recordar",est);
        }else{
            localStorage.removeItem("recordar");
        }
    });
});