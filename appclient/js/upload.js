var Upload = function (file) {
    this.file = file;
};

Upload.prototype.getType = function () {
    return this.file.type;
};
Upload.prototype.getSize = function () {
    return this.file.size;
};
Upload.prototype.getName = function () {
    return this.file.name;
};
Upload.prototype.doUpload = function (endpoint, id_input) {
    var that = this;
    var formData = new FormData();

    // add assoc key values, this will be posts values
    formData.append("uploaded_file", this.file, this.getName());
    formData.append("upload_file", true);
    var objectParent = null;

    $.ajax({
        type: "POST",
        url: baseUrl + endpoint,
        dataType: 'json',
        beforeSend: function () {
            $("#progress-wrp").addClass("show");
            $("#" + id_input + "_upload").attr("disabled", "disabled");
            objectParent = $("#" + id_input + "_upload");
            while (!objectParent.is("form") && !objectParent.is("body") && !objectParent.is("body")) {
                objectParent = objectParent.parent();
            }
            if (objectParent.is("form")) {
                $(objectParent).find("input,select,textarea,button,a").attr("disabled", "disabled");
            }
        },
        xhr: function () {
            var myXhr = $.ajaxSettings.xhr();
            if (myXhr.upload) {
                myXhr.upload.addEventListener('progress', that.progressHandling, false);
            }
            return myXhr;
        },
        success: function (data) {
            createMessage(data);
            if (data.status === 200) {
                $("#" + id_input).val(data.data);
            } else {
                $("#" + id_input).val("");
                $("#" + id_input + "_upload").val("");
            }
            console.log(data);
            $("#" + id_input + "_upload").removeAttr("disabled");
            if (objectParent.is("form")) {
                $(objectParent).find("input,select,textarea,button,a").removeAttr("disabled");
            }
            // your callback here
        },
        error: function (error) {
            console.log(error);
            $("#" + id_input + "_upload").removeAttr("disabled");
            if (objectParent.is("form")) {
                $(objectParent).find("input,select,textarea,button,a").removeAttr("disabled");
            }
            // handle error
        },
        async: true,
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        timeout: 60000
    });
};

Upload.prototype.progressHandling = function (event) {
    var percent = 0;
    var position = event.loaded || event.position;
    var total = event.total;
    var progress_bar_id = "#progress-wrp";
    if (event.lengthComputable) {
        percent = Math.ceil(position / total * 100);
    }
    // update progressbars classes so it fits your code
    $(progress_bar_id + " .progress-bar").css("width", +percent + "%");
    $(progress_bar_id + " .status").text(percent + "%");
    if (percent === 100) {
        setTimeout(function () {
            percent = 0;
            $(progress_bar_id + " .progress-bar").css("width", +percent + "%");
            $(progress_bar_id + " .status").text(percent + "%");
            $("#progress-wrp").removeClass("show");
        }, 1000);
    }
};

function buildSingleUpload(obj) {
    var id = $(obj).attr("id");
    var id_upload = id + "_upload";
    var inputFile = $("#" + id_upload);
    var type = inputFile.attr("type");
    if (type === "file") {
        inputFile.change(function (e) {
            var endpoint = inputFile.data("endpoint");
            if (!endpoint) {
                console.error("No se puede hacer un upload sin definir el endpoint");
                return false;
            }
            var url = "?c=" + endpoint;
            var action = inputFile.data("action");
            if (action) {
                url += "&a=" + action;
            }
            var flag = inputFile.data("flag");
            if (flag) {
                url += "&flag=" + flag;
            }
            var file = $(this)[0].files[0];
            var upload = new Upload(file);
            // maby check size or type here with upload.getSize() and upload.getType()
            // execute upload
            upload.doUpload(url, id);
        });
    } else {
        console.error("La referencia " + id_upload + " no es un input type file ");
    }
}