
<?php
$required = "required";
if (@$create) {
    $reset = true;
    $action = 'Create';
    $role = '';
} else {
    $required = "";
    $reset = false;
    $action = 'Detail';
    $role = "data-eprole='form'";
}
?>
<form data-reset='<?php echo $reset ?>' 
      id='frmProduct' 
      action='?c=product&a=action<?php echo $action ?>' 
      method='POST' 
      <?php echo $role ?>
      ><div class='row'>
        <div class='col-sm-12'>
            <div class='form-group'>
                <h2>Datos de Producto</h2>
            </div>
        </div>
    </div><div class='row'><div class='col-sm-x'>
            <div class='form-group'>
                <!--<label for='prdId'>prdId:</label>-->
                <input readonly 
                       minlength='1' 
                       maxlength='100000' 
                       type='hidden' 
                       class='form-control' 
                       id='prdId' 
                       name='prdId' 
                       /></div>
        </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='prdName'>Nombre:</label><input  
                    minlength='1' 
                    maxlength='30' 
                    type='input' 
                    class='form-control' 
                    id='prdName' 
                    name='prdName' 
                    required /></div>
        </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='prdDescription'>Descripcion:</label><textarea  
                    class='form-control' 
                    id='prdDescription' 
                    name='prdDescription' 
                    required ></textarea></div>
        </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='prdImagen'>Imagen:</label>
                <input type="hidden" 
                       name="prdImagen" id="prdImagen" 
                       data-eprole="upload" 
                       data-accept="image/*"
                       data-endpoint="upload"
                       data-action=""
                       data-flag="product"
                       <?php echo $required ?> />
            </div>
        </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='tpId'>Tipo de producto:</label><select required class='form-control'id='tpId'name='tpId'><option value=''>[SELECCIONE OPCION]</option><?php
                    foreach ($type_products as $entity) {
                        echo "<option value='{$entity->getId()}'>{$entity->describeStr()}</option>";
                    }
                    ?></select></div>
        </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='proId'>Proveedor:</label><select required class='form-control'id='proId'name='proId'><option value=''>[SELECCIONE OPCION]</option><?php
                    foreach ($providers as $entity) {
                        echo "<option value='{$entity->getId()}'>{$entity->describeStr()}</option>";
                    }
                    ?></select></div>
        </div></div><div class='row'>
        <div class='col-sm-12'>
            <button type='submit' 
                    class='btn btn-success' 
                    data-form='frmProduct'>Guardar</button>
                    <?php
                    if (@$create) {
                        ?>
                <button type='reset' 
                        class='btn btn-danger' 
                        >Limpiar</button>
                        <?php
                    } else {
                        ?>
                <a href='?c=product&a=viewCreate' class='btn btn-info'>Nuevo registro</a>
                <?php
            }
            ?>
            <a href='?c=product' class='btn btn-warning'>Ver todos los registros</a>

        </div>
        <hr class='d-sm-none'>
    </div></form>


<?php
if (!@$create) {
    ?>
    <br><br><br>
    <div class='row'>
    <div class='col-sm-12'>
        <h2>Lista de Certificados</h2>
        <br>
        <div data-eprole='list'data-paginator='true' data-endpoint='?c=cert_product&a=actionListByProduct&proId=<?php echo $_REQUEST["acc"] ?>' data-cb='printDataSource'>
<!--            <form  action='#' data-eprole='filter'>
                <div class='row'>
                    <div class='col-3'>
                        <input placeholder='Buscar...' class='form-control' type='search' id='search' name='search' value='' />
                    </div>
                    <div class='col-2'>
                        <button class='btn btn-danger'>Buscar</button>
                    </div>
                </div>
            </form>-->
            <br>
            <table class='table table-striped'>
                <thead class='bg-primary navbar-dark text-white'>
                    <tr>
                        <!--<th data-property='cerId'>cerId</th>-->
                        <th data-property='cerTitle'>Titulo</th>
                        <th data-property='cerCode'>Codigo</th>
                        <th data-property='cerDescription'>Descripcion</th>
                        <th data-property='url' data-get="loadUrlDoc">Archivo</th>
                        <th data-btns='delete,edit'>Opciones</th>
                    </tr>
                </thead>
                <tbody class='list'></tbody>
            </table><nav></nav></div>
        <a href='?c=cert_product&a=viewCreate' class='btn btn-info'>Asociar registro</a></div></div>
    <?php
}
?>