
<?php
$required = "required";
if (@$create) {
    $reset = true;
    $action = 'Create';
    $role = '';
} else {
    $reset = false;
    $action = 'Detail';
    $role = "data-eprole='form'";
    $required = "";
}
?>
<form data-reset='<?php echo $reset ?>' 
      id='frmGallery_item' 
      action='?c=gallery_item&a=action<?php echo $action ?>' 
      method='POST' 
      <?php echo $role ?>
      ><div class='row'>
        <div class='col-sm-12'>
            <div class='form-group'>
                <h2>Datos de Item de Galeria</h2>
            </div>
        </div>
    </div>
    <div class='row'><div class='col-sm-x'>
            <div class='form-group'>
                <!--<label for='gtmId'>gtmId:</label>-->
                <input readonly 
                       minlength='1' 
                       maxlength='100000' 
                       type='hidden' 
                       class='form-control' 
                       id='gtmId' 
                       name='gtmId' 
                       />
            </div>
        </div>
        <div class='col-sm-4'>
            <div class='form-group'>
                <label for='gtmTitle'>Titulo:</label><input  
                    minlength='1' 
                    maxlength='45' 
                    type='input' 
                    class='form-control' 
                    id='gtmTitle' 
                    name='gtmTitle' 
                    required /></div>
        </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='gtmDescription'>Descripcion:</label><input  
                    minlength='1' 
                    maxlength='100' 
                    type='input' 
                    class='form-control' 
                    id='gtmDescription' 
                    name='gtmDescription' 
                    /></div>
        </div>
        <div class='col-sm-4'>
            <div class='form-group'>
                <label for='gtmImage'>Imagen:</label>
                <input type="hidden" 
                       name="gtmImage" id="gtmImage" data-eprole="upload" 
                       data-accept="image/*"
                       data-endpoint="upload"
                       data-action=""
                       data-flag="gallery"
                       <?php echo $required; ?> />
            </div>
        </div>

    </div><div class='row'>
        <div class='col-sm-12'>
            <button type='submit' 
                    class='btn btn-success' 
                    data-form='frmGallery_item'>Guardar</button>
                    <?php
                    if (@$create) {
                        ?>
                <button type='reset' 
                        class='btn btn-danger' 
                        >Limpiar</button>
                        <?php
                    } else {
                        ?>
                <a href='?c=gallery_item&a=viewCreate' class='btn btn-info'>Nuevo registro</a>
                <?php
            }
            ?>
            <a href='?c=gallery_item' class='btn btn-warning'>Ver todos los registros</a>

        </div>
        <hr class='d-sm-none'>
    </div>
</form>