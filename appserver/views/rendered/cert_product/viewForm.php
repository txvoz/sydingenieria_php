
<?php
if (@$create) {
    $reset = true;
    $action = 'Create';
    $role = '';
} else {
    $reset = false;
    $action = 'Detail';
    $role = "data-eprole='form'";
}
?>
<form data-reset='<?php echo $reset ?>' 
      id='frmCert_product' 
      action='?c=cert_product&a=action<?php echo $action ?>' 
      method='POST' 
      <?php echo $role ?>
      ><div class='row'>
        <div class='col-sm-12'>
            <div class='form-group'>
                <h2>Datos de Asociacion de Certificado Por Producto</h2>
            </div>
        </div>
    </div><div class='row'>
        <div class='col-sm-x'>
            <div class='form-group'>
                <!--<label for='cpId'>cpId:</label>-->
                <input readonly 
                       minlength='1' 
                       maxlength='100000' 
                       type='hidden' 
                       class='form-control' 
                       id='cpId' 
                       name='cpId' 
                       /></div>
        </div>
        <div class='col-sm-4'>
            <div class='form-group'>
                <label for='prdId'>Producto:</label>
                <select required class='form-control'id='prdId'name='prdId'><option value=''>[SELECCIONE OPCION]</option><?php
                    foreach ($products as $entity) {
                        echo "<option value='{$entity->getId()}'>{$entity->describeStr()}</option>";
                    }
                    ?>
                </select>
            </div>
        </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='cerId'>Certificado:</label>
                <select required class='form-control'id='cerId'name='cerId'>
                    <option value=''>[SELECCIONE OPCION]</option>
                        <?php
                    foreach ($certs as $entity) {
                        echo "<option value='{$entity->getId()}'>{$entity->describeStr()}</option>";
                    }
                    ?>
                </select>
            </div>
        </div></div><div class='row'>
        <div class='col-sm-12'>
            <button type='submit' 
                    class='btn btn-success' 
                    data-form='frmCert_product'>Guardar</button>
<?php
if (@$create) {
    ?>
                <button type='reset' 
                        class='btn btn-danger' 
                        >Limpiar</button>
                <?php
            } else {
                ?>
                <a href='?c=cert_product&a=viewCreate' class='btn btn-info'>Nuevo registro</a>
    <?php
}
?>
            <a href='?c=cert_product' class='btn btn-warning'>Ver todos los registros</a>

        </div>
        <hr class='d-sm-none'>
    </div></form>