<div class='row'>
    <div class='col-sm-12'>
        <h2>Lista de Certificados Por Producto</h2>
        <br>
        <div data-eprole='list'data-paginator='true' data-endpoint='?c=cert_product&a=actionList' data-cb='printDataSource'>
            <form  action='#' data-eprole='filter'>
                <div class='row'>
                    <div class='col-3'>
                        <input placeholder='Buscar...' class='form-control' type='search' id='search' name='search' value='' />
                    </div>
                    <div class='col-2'>
                        <button class='btn btn-danger'>Buscar</button>
                    </div>
                </div>
            </form><br>
            <table class='table table-striped'><thead class='bg-primary navbar-dark text-white'><tr>
                        <!--<th data-property='cpId'>cpId</th>-->
                        <th data-property='fkproduct.prdName'>Producto</th>
                        <th data-property='fkproduct.prdDescription'>Descripcion Producto</th>
                        <th data-property='fkcert.cerTitle'>Certificado</th>
                        <th data-property='fkcert.cerDescription'>Descripcion Certificado</th>
                        <th data-property='fkcert.url' data-get="loadUrlDoc">Archivo</th>
                        <th data-btns='delete,edit,*detail'>Opciones</th></tr></thead><tbody class='list'></tbody>
            </table>
            <nav></nav>
        </div>
        <a href='?c=cert_product&a=viewCreate' class='btn btn-info'>Nuevo registro</a></div></div>