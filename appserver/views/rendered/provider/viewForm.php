
<?php
if (@$create) {
    $reset = true;
    $action = 'Create';
    $role = '';
} else {
    $reset = false;
    $action = 'Detail';
    $role = "data-eprole='form'";
}
?>
<form data-reset='<?php echo $reset ?>' 
      id='frmProvider' 
      action='?c=provider&a=action<?php echo $action ?>' 
      method='POST' 
      <?php echo $role ?>
      ><div class='row'>
        <div class='col-sm-12'>
            <div class='form-group'>
                <h2>Datos de Proveedor</h2>
            </div>
        </div>
    </div>
    <div class='row'>
        <div class='col-sm-x'>
            <div class='form-group'>
                <!--<label for='proId'>proId:</label>-->
                <input readonly 
                       minlength='1' 
                       maxlength='100000' 
                       type='hidden' 
                       class='form-control' 
                       id='proId' 
                       name='proId' 
                       /></div>
        </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='proNit'>Nit:</label><input  
                    minlength='1' 
                    maxlength='45' 
                    type='input' 
                    class='form-control' 
                    id='proNit' 
                    name='proNit' 
                    /></div>
        </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='proName'>Nombre:</label><input  
                    minlength='1' 
                    maxlength='45' 
                    type='input' 
                    class='form-control' 
                    id='proName' 
                    name='proName' 
                    required /></div>
        </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='proDescription'>Descripcion:</label><textarea  
                    class='form-control' 
                    id='proDescription' 
                    name='proDescription' 
                    required ></textarea></div>
        </div>
        <div class='col-sm-4'>
            <div class='form-group'>
                <label for='proLogo'>Logo:</label>
                <input type="hidden" 
                       name="proLogo" id="proLogo" 
                       data-eprole="upload" 
                       data-accept="image/*"
                       data-endpoint="upload"
                       data-action=""
                       data-flag="provider"
                       required />
            </div>
        </div>
        <div class='col-sm-4'>
            <div class='form-group'>
                <label for='proWebSite'>Sitio web:</label><input  
                    minlength='1' 
                    maxlength='45' 
                    type='url' 
                    class='form-control' 
                    id='proWebSite' 
                    name='proWebSite' 
                    />
            </div>
        </div>
    </div>
    <div class='row'>
        <div class='col-sm-12'>
            <button type='submit' 
                    class='btn btn-success' 
                    data-form='frmProvider'>Guardar</button>
                    <?php
                    if (@$create) {
                        ?>
                <button type='reset' 
                        class='btn btn-danger' 
                        >Limpiar</button>
                        <?php
                    } else {
                        ?>
                <a href='?c=provider&a=viewCreate' class='btn btn-info'>Nuevo registro</a>
                <?php
            }
            ?>
            <a href='?c=provider' class='btn btn-warning'>Ver todos los registros</a>

        </div>
        <hr class='d-sm-none'>
    </div></form>