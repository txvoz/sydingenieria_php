<div class='row'><div class='col-sm-12'><h2>Lista de Proveedores</h2><br><div data-eprole='list'data-paginator='true' data-endpoint='?c=provider&a=actionList' data-cb='printDataSource'><form  action='#' data-eprole='filter'>
                <div class='row'>
                    <div class='col-3'>
                        <input placeholder='Buscar...' class='form-control' type='search' id='search' name='search' value='' />
                    </div>
                    <div class='col-2'>
                        <button class='btn btn-danger'>Buscar</button>
                    </div>
                </div>
            </form><br><table class='table table-striped'><thead class='bg-primary navbar-dark text-white'>
                    <tr>
                        <!--<th data-property='proId'>proId</th>-->
                        <th data-property='proNit'>Nit</th>
                        <th data-property='proName'>Nombre</th>
                        <th data-property='proDescription'>Descripcion</th>
                        <th data-property='url' data-get='loadUrl'>Logo</th>
                        <th data-property='proWebSite' data-get='loadUrlWeb' >Sitio web</th>
                        <th data-btns='delete,edit,*detail'>Opciones</th></tr></thead><tbody class='list'></tbody></table><nav></nav></div><a href='?c=provider&a=viewCreate' class='btn btn-info'>Nuevo registro</a></div></div>