
<?php
if (@$create) {
    $reset = true;
    $action = 'Create';
    $role = '';
} else {
    $reset = false;
    $action = 'Detail';
    $role = "data-eprole='form'";
}
?>
<form data-reset='<?php echo $reset ?>' 
      id='frmUser' 
      action='?c=user&a=action<?php echo $action ?>' 
      method='POST' 
      <?php echo $role ?>
      ><div class='row'>
        <div class='col-sm-12'>
            <div class='form-group'>
                <h2>Datos de Usuario</h2>
            </div>
        </div>
    </div><div class='row'><div class='col-sm-x'>
            <div class='form-group'>
                <!--<label for='useId'>useId:</label>-->
                <input readonly 
                       minlength='1' 
                       maxlength='100000' 
                       type='hidden' 
                       class='form-control' 
                       id='useId' 
                       name='useId' 
                       /></div>
        </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='useEmail'>Correo:</label><input  
                    minlength='1' 
                    maxlength='45' 
                    type='input' 
                    class='form-control' 
                    id='useEmail' 
                    name='useEmail' 
                    required /></div>
        </div>
        <div class='col-sm-4'>
            <div class='form-group'>
                <label for='useName'>Nombre:</label><input  
                    minlength='1' 
                    maxlength='30' 
                    type='input' 
                    class='form-control' 
                    id='useName' 
                    name='useName' 
                    required /></div>
        </div>
        <div class='col-sm-4'>
            <div class='form-group'>
                <label for='usePassword'>Contraseña:</label><input  
                    minlength='1' 
                    maxlength='45' 
                    type='password' 
                    class='form-control' 
                    id='usePassword' 
                    name='usePassword' 
                    required /></div>
        </div>
    </div><div class='row'>
        <div class='col-sm-12'>
            <button type='submit' 
                    class='btn btn-success' 
                    data-form='frmUser'>Guardar</button>
                    <?php
                    if (@$create) {
                        ?>
                <button type='reset' 
                        class='btn btn-danger' 
                        >Limpiar</button>
                        <?php
                    } else {
                        ?>
                <a href='?c=user&a=viewCreate' class='btn btn-info'>Nuevo registro</a>
                <?php
            }
            ?>
            <a href='?c=user' class='btn btn-warning'>Ver todos los registros</a>

        </div>
        <hr class='d-sm-none'>
    </div></form>