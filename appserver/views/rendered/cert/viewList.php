<div class='row'><div class='col-sm-12'><h2>Lista de Certificados</h2><br><div data-eprole='list'data-paginator='true' data-endpoint='?c=cert&a=actionList' data-cb='printDataSource'><form  action='#' data-eprole='filter'>
                <div class='row'>
                    <div class='col-3'>
                        <input placeholder='Buscar...' class='form-control' type='search' id='search' name='search' value='' />
                    </div>
                    <div class='col-2'>
                        <button class='btn btn-danger'>Buscar</button>
                    </div>
                </div>
            </form><br><table class='table table-striped'><thead class='bg-primary navbar-dark text-white'><tr>
                        <!--<th data-property='cerId'>cerId</th>-->
                        <th data-property='cerTitle'>Titulo</th>
                        <th data-property='cerCode'>Codigo</th>
                        <th data-property='cerDescription'>Descripcion</th>
                        <th data-property='url' data-get="loadUrlDoc">Archivo</th>
                        <th data-btns='delete,edit,*detail'>Opciones</th></tr></thead><tbody class='list'></tbody></table><nav></nav></div><a href='?c=cert&a=viewCreate' class='btn btn-info'>Nuevo registro</a></div></div>