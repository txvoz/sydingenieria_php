
<?php
$required = "required";
if (@$create) {
    $reset = true;
    $action = 'Create';
    $role = '';
} else {
    $required = "";
    $reset = false;
    $action = 'Detail';
    $role = "data-eprole='form'";
}
?>
<form data-reset='<?php echo $reset ?>' 
      id='frmCert' 
      action='?c=cert&a=action<?php echo $action ?>' 
      method='POST' 
      <?php echo $role ?>
      >
    <div class='row'>
        <div class='col-sm-12'>
            <div class='form-group'>
                <h2>Datos de Certificado</h2>
            </div>
        </div>
    </div><div class='row'><div class='col-sm-x'>
            <div class='form-group'>
                <!--<label for='cerId'>Id:</label>-->
                <input readonly 
                       minlength='1' 
                       maxlength='100000' 
                       type='hidden' 
                       class='form-control' 
                       id='cerId' 
                       name='cerId' 
                       /></div>
        </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='cerTitle'>Titulo:</label><input  
                    minlength='1' 
                    maxlength='45' 
                    type='input' 
                    class='form-control' 
                    id='cerTitle' 
                    name='cerTitle' 
                    required /></div>
        </div><div class='col-sm-4'>
            <div class='form-group'>
                <label for='cerCode'>Codigo:</label><input  
                    minlength='1' 
                    maxlength='45' 
                    type='input' 
                    class='form-control' 
                    id='cerCode' 
                    name='cerCode' 
                    required />
            </div>
        </div>
        <div class='col-sm-4'>
            <div class='form-group'>
                <label for='cerFile'>Archivo:</label>
                 <input type="hidden" 
                       name="cerFile" id="cerFile" 
                       data-eprole="upload" 
                       data-accept="application/pdf"
                       data-endpoint="upload"
                       data-action=""
                       data-flag="cert"
                       <?php echo $required ?> />
            </div>
        </div>
        <div class='col-sm-4'>
            <div class='form-group'>
                <label for='cerDescription'>Descripcion:</label><textarea  
                    class='form-control' 
                    id='cerDescription' 
                    name='cerDescription' 
                    required ></textarea>
            </div>
        </div>
    </div>
    
    <div class='row'>
        <div class='col-sm-12'>
            <button type='submit' 
                    class='btn btn-success' 
                    data-form='frmCert'>Guardar</button>
                    <?php
                    if (@$create) {
                        ?>
                <button type='reset' 
                        class='btn btn-danger' 
                        >Limpiar</button>
                        <?php
                    } else {
                        ?>
                <a href='?c=cert&a=viewCreate' class='btn btn-info'>Nuevo registro</a>
                <?php
            }
            ?>
            <a href='?c=cert' class='btn btn-warning'>Ver todos los registros</a>

        </div>
        <hr class='d-sm-none'>
    </div></form>