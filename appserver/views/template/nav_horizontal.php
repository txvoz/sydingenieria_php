<?php
$c = Config::singleton();
?>
<div id="myMenu">
    <nav class="navbar navbar-toggleable-md navbar-inverse fixed-top navbar navbar-expand-sm <?php echo $c->get("pclass") ?> navbar-dark text-white">
        <a class="navbar-brand" href="?">Inicio</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class='nav nav-pills'>
                
                <li class='nav-item dropdown'>
                    <a class='nav-link dropdown-toggle text-white' data-toggle='dropdown' href='#'>Productos</a>
                    <div class='dropdown-menu'>
                        <a class='dropdown-item' href='?c=product&a=viewCreate'>Nuevo registro</a>
                        <a class='dropdown-item' href='?c=product'>Listar registros</a>
                    </div>
                </li>                
                
                <li class='nav-item dropdown'>
                    <a class='nav-link dropdown-toggle text-white' data-toggle='dropdown' href='#'>Proyectos</a>
                    <div class='dropdown-menu'>
                        <a class='dropdown-item' href='?c=project&a=viewCreate'>Nuevo registro</a>
                        <a class='dropdown-item' href='?c=project'>Listar registros</a>
                    </div>
                </li>

                <li class='nav-item dropdown'>
                    <a class='nav-link dropdown-toggle text-white' data-toggle='dropdown' href='#'>Proveedores</a>
                    <div class='dropdown-menu'>
                        <a class='dropdown-item' href='?c=provider&a=viewCreate'>Nuevo registro</a>
                        <a class='dropdown-item' href='?c=provider'>Listar registros</a>
                    </div>
                </li>                
                
                <li class='nav-item dropdown'>
                    <a class='nav-link dropdown-toggle text-white' data-toggle='dropdown' href='#'>Servicios</a>
                    <div class='dropdown-menu'>
                        <a class='dropdown-item' href='?c=service&a=viewCreate'>Nuevo registro</a>
                        <a class='dropdown-item' href='?c=service'>Listar registros</a>
                    </div>
                </li>                
                
                <li class='nav-item dropdown'>
                    <a class='nav-link dropdown-toggle text-white' data-toggle='dropdown' href='#'>Aliados</a>
                    <div class='dropdown-menu'>
                        <a class='dropdown-item' href='?c=allying&a=viewCreate'>Nuevo registro</a>
                        <a class='dropdown-item' href='?c=allying'>Listar registros</a>
                    </div>
                </li>                

                <li class='nav-item dropdown'>
                    <a class='nav-link dropdown-toggle text-white' data-toggle='dropdown' href='#'>Certificados</a>
                    <div class='dropdown-menu'>
                        <a class='dropdown-item' href='?c=cert&a=viewCreate'>Nuevo certificado</a>
                        <a class='dropdown-item' href='?c=cert'>Listar certificados</a>
                        <a class='dropdown-item' href='?c=cert_product&a=viewCreate'>Nuevo certificado por producto</a>
                        <a class='dropdown-item' href='?c=cert_product'>Listar certificados por producto</a>
                    </div>
                </li>               
                
                <li class='nav-item dropdown'>
                    <a class='nav-link dropdown-toggle text-white' data-toggle='dropdown' href='#'>Tipos de productos</a>
                    <div class='dropdown-menu'>
                        <a class='dropdown-item' href='?c=type_product&a=viewCreate'>Nuevo registro</a>
                        <a class='dropdown-item' href='?c=type_product'>Listar registros</a>
                    </div>
                </li>                

                <li class='nav-item dropdown'>
                    <a class='nav-link dropdown-toggle text-white' data-toggle='dropdown' href='#'>Contactos</a>
                    <div class='dropdown-menu'>
                        <a class='dropdown-item' href='?c=contact&a=viewCreate'>Nuevo registro</a>
                        <a class='dropdown-item' href='?c=contact'>Listar registros</a>
                    </div>
                </li>                

                <li class='nav-item dropdown'>
                    <a class='nav-link dropdown-toggle text-white' data-toggle='dropdown' href='#'>Galeria</a>
                    <div class='dropdown-menu'>
                        <a class='dropdown-item' href='?c=gallery_item&a=viewCreate'>Nuevo registro</a>
                        <a class='dropdown-item' href='?c=gallery_item'>Listar registros</a>
                    </div>
                </li>                                

                <li class='nav-item dropdown'>
                    <a class='nav-link dropdown-toggle text-white' data-toggle='dropdown' href='#'>Usuarios</a>
                    <div class='dropdown-menu'>
                        <a class='dropdown-item' href='?c=user&a=viewCreate'>Nuevo registro</a>
                        <a class='dropdown-item' href='?c=user'>Listar registros</a>
                    </div>
                </li>           
            </ul>
        </div>  
    </nav>
</div>