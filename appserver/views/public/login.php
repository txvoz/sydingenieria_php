<div class="row">
    <div class="col-sm-4"></div>
    <div class="col-sm-4">
        <form data-reset="true" 
              id="frmLogin" 
              action="?c=Auth&a=actionLogin"
              data-success="successLogin"
              method="POST">
            <!--<form action="?a=validar" method="get">-->
            <div class="form-group">
                <h2>Ingreso</h2>
            </div>
            <div class="form-group">
                <label for="correo">Correo:</label>
                <input type="email" class="form-control" id="useEmail" name="useEmail" required>
            </div>
            <div class="form-group">
                <label for="pwd">Contraseña:</label>
                <input type="password" class="form-control" id="usePassword" name="usePassword" required>
            </div>
            <div class="form-group form-check">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" id="recordar"> Recordar credenciales
                </label>
            </div>
            <button type="submit" 
                    class="btn btn-primary"
                    data-form="frmLogin">Enviar</button>
        </form>
        <hr class="d-sm-none">
    </div>
    <div class="col-sm-4"></div>
</div>
<br>
<br>