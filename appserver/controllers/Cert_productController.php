<?php

class Cert_productController implements IController, IManagementForm, IAction {

    private $path = "rendered/cert_product/";
    private $config = null;

    public function __construct() {
        $this->config = Config::singleton();
        require "{$this->config->get("entities")}Cert_product.php";
        require "{$this->config->get("models")}Cert_productModel.php";
        require "{$this->config->get("entities")}Cert.php";
        require "{$this->config->get("models")}CertModel.php";
        require "{$this->config->get("entities")}Product.php";
        require "{$this->config->get("models")}ProductModel.php";
    }

    public function index() {
        $this->viewList();
    }

    public function viewCreate() {
        $vars = [];
        $vars["create"] = true;
        $model1 = new CertModel();
        $vars['certs'] = $model1->get()->data;
        $model2 = new ProductModel();
        $vars['products'] = $model2->get()->data;
        View::show("{$this->path}viewForm", $vars);
    }

    public function viewDetail() {
        $vars = [];
        $vars["id"] = $_REQUEST["acc"];
        $model1 = new CertModel();
        $vars['certs'] = $model1->get()->data;
        $model2 = new ProductModel();
        $vars['products'] = $model2->get()->data;
        View::show("{$this->path}viewForm", $vars);
    }

    public function viewList() {
        View::show("{$this->path}viewList");
    }

    public function actionListByProduct() {
        $arg = new stdClass();
        //*******************
        $m = new Cert_productModel();
        $proId = @$_REQUEST["proId"];
        $arg->filtro = @$_REQUEST['search'];
        $arg->paginator = null;
        $rcount = $m->getCountByIdProd($arg->filtro,$proId)->cantidad;
        $arg->paginator = new Paginator($rcount, @$_REQUEST['p']);
        $r = $m->getByProdId($arg,$proId,false);
        $m->lazyLoad($r->data);
        if ($arg->paginator !== null) {
            $r->paginator = $arg->paginator->renderPaginator();
        }
        $r->count = $rcount;
        echo json_encode($r);
    }
    
    public function actionList() {
        $arg = new stdClass();
        //*******************
        $m = new Cert_productModel();
        $arg->filtro = @$_REQUEST['search'];
        $arg->paginator = null;
        $rcount = $m->getCount($arg->filtro)->cantidad;
        $arg->paginator = new Paginator($rcount, @$_REQUEST['p']);
        $r = $m->get($arg, false);
        $m->lazyLoad($r->data);
        if ($arg->paginator !== null) {
            $r->paginator = $arg->paginator->renderPaginator();
        }
        $r->count = $rcount;
        echo json_encode($r);
    }

    public function actionListData() {
        //*******************
        $m = new Cert_productModel();
        $rcount = $m->getCount()->cantidad;
        $r = $m->get();
        $r->count = $rcount;
        echo json_encode($r);
    }

    public function actionDetail() {
        if ($_SERVER['REQUEST_METHOD'] === "GET") {
            $r = null;
            $m = new Cert_productModel();
            $e = new Cert_product();
            $e->setCpId($_REQUEST["acc"]);
            $r = $m->getById($e);
            echo json_encode($r);
        } else if ($_SERVER['REQUEST_METHOD'] === "POST") {
            $this->actionUpdate();
        }
    }

    public function actionCreate() {
        $data = Utils::getParamsByBody();
        $e = new Cert_product();
        $e->serializeByObject($data);
        $m = new Cert_productModel();
        $r = $m->insert($e);
        echo json_encode($r);
    }

    public function actionDelete() {
        $m = new Cert_productModel();
        $e = new Cert_product();
        $e->setCpId($_REQUEST["acc"]);
        $r = $m->delete($e);
        echo json_encode($r);
    }

    public function actionUpdate() {
        $data = Utils::getParamsByBody();
        $e = new Cert_product();
        $e->serializeByObject($data);
        $m = new Cert_productModel();
        $r = $m->update($e);
        echo json_encode($r);
    }

}
