<?php

class CertController implements IController, IManagementForm, IAction {

    private $path = "rendered/cert/";
    private $config = null;

    public function __construct() {
        $this->config = Config::singleton();
        require "{$this->config->get("entities")}Cert.php";
        require "{$this->config->get("models")}CertModel.php";
    }

    public function index() {
        $this->viewList();
    }

    public function viewCreate() {
        $vars = [];
        $vars["create"] = true;

        View::show("{$this->path}viewForm", $vars);
    }

    public function viewDetail() {
        $vars = [];
        $vars["id"] = $_REQUEST["acc"];

        View::show("{$this->path}viewForm", $vars);
    }

    public function viewList() {
        View::show("{$this->path}viewList");
    }

    public function actionList() {
        $arg = new stdClass();
        //*******************
        $m = new CertModel();
        $arg->filtro = @$_REQUEST['search'];
        $arg->paginator = null;
        $rcount = $m->getCount($arg->filtro)->cantidad;
        $arg->paginator = new Paginator($rcount, @$_REQUEST['p']);
        $r = $m->get($arg, false);
        $m->lazyLoad($r->data);
        if ($arg->paginator !== null) {
            $r->paginator = $arg->paginator->renderPaginator();
        }
        $r->count = $rcount;
        echo json_encode($r);
    }

    public function actionListData() {
        //*******************
        $m = new CertModel();
        $rcount = $m->getCount()->cantidad;
        $r = $m->get();
        $r->count = $rcount;
        echo json_encode($r);
    }

    public function actionDetail() {
        if ($_SERVER['REQUEST_METHOD'] === "GET") {
            $r = null;
            $m = new CertModel();
            $e = new Cert();
            $e->setCerId($_REQUEST["acc"]);
            $r = $m->getById($e);
            echo json_encode($r);
        } else if ($_SERVER['REQUEST_METHOD'] === "POST") {
            $this->actionUpdate();
        }
    }

    public function actionCreate() {
        $data = Utils::getParamsByBody();
        $e = new Cert();
        $e->serializeByObject($data);
        $m = new CertModel();
        $r = $m->insert($e);
        echo json_encode($r);
    }

    public function actionDelete() {
        $m = new CertModel();
        $e = new Cert();
        $e->setCerId($_REQUEST["acc"]);
        $r = $m->delete($e);
        echo json_encode($r);
    }

    public function actionUpdate() {
        $data = Utils::getParamsByBody();
        $e = new Cert();
        $e->serializeByObject($data);
        $m = new CertModel();
        $r = $m->update($e);
        echo json_encode($r);
    }

}
