<?php

class ProductController implements IController, IManagementForm, IAction {

    private $path = "rendered/product/";
    private $config = null;

    public function __construct() {
        $this->config = Config::singleton();
        require "{$this->config->get("entities")}Product.php";
        require "{$this->config->get("models")}ProductModel.php";
        require "{$this->config->get("entities")}Provider.php";
        require "{$this->config->get("models")}ProviderModel.php";
        require "{$this->config->get("entities")}Type_product.php";
        require "{$this->config->get("models")}Type_productModel.php";
    }

    public function index() {
        $this->viewList();
    }

    public function viewCreate() {
        $vars = [];
        $vars["create"] = true;
        $model1 = new ProviderModel();
        $vars['providers'] = $model1->get()->data;
        $model2 = new Type_productModel();
        $vars['type_products'] = $model2->get()->data;
        View::show("{$this->path}viewForm", $vars);
    }

    public function viewDetail() {
        $vars = [];
        $vars["id"] = $_REQUEST["acc"];
        $model1 = new ProviderModel();
        $vars['providers'] = $model1->get()->data;
        $model2 = new Type_productModel();
        $vars['type_products'] = $model2->get()->data;
        View::show("{$this->path}viewForm", $vars);
    }

    public function viewList() {
        View::show("{$this->path}viewList");
    }

    public function actionList() {
        $arg = new stdClass();
        //*******************
        $m = new ProductModel();
        $arg->filtro = @$_REQUEST['search'];
        $arg->paginator = null;
        $rcount = $m->getCount($arg->filtro)->cantidad;
        $arg->paginator = new Paginator($rcount, @$_REQUEST['p']);
        $r = $m->get($arg, false);
        $m->lazyLoad($r->data);
        if ($arg->paginator !== null) {
            $r->paginator = $arg->paginator->renderPaginator();
        }
        $r->count = $rcount;
        echo json_encode($r);
    }

    public function actionListData() {
        //*******************
        $m = new ProductModel();
        $rcount = $m->getCount()->cantidad;
        $r = $m->get();
        $r->count = $rcount;
        echo json_encode($r);
    }

    public function actionDetail() {
        if ($_SERVER['REQUEST_METHOD'] === "GET") {
            $r = null;
            $m = new ProductModel();
            $e = new Product();
            $e->setPrdId($_REQUEST["acc"]);
            $r = $m->getById($e);
            echo json_encode($r);
        } else if ($_SERVER['REQUEST_METHOD'] === "POST") {
            $this->actionUpdate();
        }
    }

    public function actionCreate() {
        $data = Utils::getParamsByBody();
        $e = new Product();
        $e->serializeByObject($data);
        $m = new ProductModel();
        $r = $m->insert($e);
        echo json_encode($r);
    }

    public function actionDelete() {
        $m = new ProductModel();
        $e = new Product();
        $e->setPrdId($_REQUEST["acc"]);
        $r = $m->delete($e);
        echo json_encode($r);
    }

    public function actionUpdate() {
        $data = Utils::getParamsByBody();
        $e = new Product();
        $e->serializeByObject($data);
        $m = new ProductModel();
        $r = $m->update($e);
        echo json_encode($r);
    }

}
