<?php

class UserController implements IController, IManagementForm, IAction {

    private $path = "rendered/user/";
    private $config = null;

    public function __construct() {
        $this->config = Config::singleton();
        require "{$this->config->get("entities")}User.php";
        require "{$this->config->get("models")}UserModel.php";
    }

    public function index() {
        $this->viewList();
    }

    public function viewCreate() {
        $vars = [];
        $vars["create"] = true;

        View::show("{$this->path}viewForm", $vars);
    }

    public function viewDetail() {
        $vars = [];
        $vars["id"] = $_REQUEST["acc"];

        View::show("{$this->path}viewForm", $vars);
    }

    public function viewList() {
        View::show("{$this->path}viewList");
    }

    public function actionList() {
        $arg = new stdClass();
        //*******************
        $m = new UserModel();
        $arg->filtro = @$_REQUEST['search'];
        $arg->paginator = null;
        $rcount = $m->getCount($arg->filtro)->cantidad;
        $arg->paginator = new Paginator($rcount, @$_REQUEST['p']);
        $r = $m->get($arg, false);
        $m->lazyLoad($r->data);
        if ($arg->paginator !== null) {
            $r->paginator = $arg->paginator->renderPaginator();
        }
        $r->count = $rcount;
        echo json_encode($r);
    }

    public function actionListData() {
        //*******************
        $m = new UserModel();
        $rcount = $m->getCount()->cantidad;
        $r = $m->get();
        $r->count = $rcount;
        echo json_encode($r);
    }

    public function actionDetail() {
        if ($_SERVER['REQUEST_METHOD'] === "GET") {
            $r = null;
            $m = new UserModel();
            $e = new User();
            $e->setUseId($_REQUEST["acc"]);
            $r = $m->getById($e);
            echo json_encode($r);
        } else if ($_SERVER['REQUEST_METHOD'] === "POST") {
            $this->actionUpdate();
        }
    }

    public function actionCreate() {
        $data = Utils::getParamsByBody();
        $e = new User();
        $e->serializeByObject($data);
        $m = new UserModel();
        $r = $m->insert($e);
        echo json_encode($r);
    }

    public function actionDelete() {
        $m = new UserModel();
        $e = new User();
        $e->setUseId($_REQUEST["acc"]);
        $r = $m->delete($e);
        echo json_encode($r);
    }

    public function actionUpdate() {
        $data = Utils::getParamsByBody();
        $e = new User();
        $e->serializeByObject($data);
        $m = new UserModel();
        $r = $m->update($e);
        echo json_encode($r);
    }

}
