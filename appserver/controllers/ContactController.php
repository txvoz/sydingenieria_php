<?php
class ContactController implements IController, IManagementForm, IAction  {
private $path = "rendered/contact/";private $config = null;public function __construct() {
        $this->config = Config::singleton();
        require "{$this->config->get("entities")}Contact.php";
        require "{$this->config->get("models")}ContactModel.php";
        }public function index() {
        $this->viewList();
        }public function viewCreate() {
        $vars = [];
        $vars["create"] = true;
        
        View::show("{$this->path}viewForm", $vars);
        }public function viewDetail() {
        $vars = [];
        $vars["id"] = $_REQUEST["acc"];
        
        View::show("{$this->path}viewForm", $vars);
        }public function viewList() {
        View::show("{$this->path}viewList");
        }public function actionList() {
        $arg = new stdClass();
        //*******************
        $m = new ContactModel();
        $arg->filtro = @$_REQUEST['search'];
        $arg->paginator = null;
        $rcount = $m->getCount($arg->filtro)->cantidad;
        $arg->paginator = new Paginator($rcount, @$_REQUEST['p']);
        $r = $m->get($arg, false);
        $m->lazyLoad($r->data);
        if ($arg->paginator !== null) {
            $r->paginator = $arg->paginator->renderPaginator();
        }
        $r->count = $rcount;
        echo json_encode($r);
        }public function actionListData() {
        //*******************
        $m = new ContactModel();
        $rcount = $m->getCount()->cantidad;
        $r = $m->get();
        $r->count = $rcount;
        echo json_encode($r);
        }public function actionDetail() {
            if ($_SERVER['REQUEST_METHOD'] === "GET") {
                $r = null;
                $m = new ContactModel();
                $e = new Contact();
                $e->setConId($_REQUEST["acc"]);
                $r = $m->getById($e);
                echo json_encode($r);
            } else if ($_SERVER['REQUEST_METHOD'] === "POST") {
                $this->actionUpdate();
            }
        }public function actionCreate() {
        $data = Utils::getParamsByBody();
        $e = new Contact();
        $e->serializeByObject($data);
        $m = new ContactModel();
        $r = $m->insert($e);
        echo json_encode($r);
        }public function actionDelete() {
        $m = new ContactModel();
        $e = new Contact();
        $e->setConId($_REQUEST["acc"]);
        $r = $m->delete($e);
        echo json_encode($r);
        }public function actionUpdate() {
        $data = Utils::getParamsByBody();
        $e = new Contact();
        $e->serializeByObject($data);
        $m = new ContactModel();
        $r = $m->update($e);
        echo json_encode($r);
        }
}