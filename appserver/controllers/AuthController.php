<?php

class AuthController implements IController {

    private $config = null;

    public function __construct() {
        $this->config = Config::singleton();
        require "{$this->config->get("entities")}User.php";
        require "{$this->config->get("models")}UserModel.php";
    }

    public function index() {
        
    }

    public function actionLogin() {
        $r = null;
        $m = new UserModel();
        $e = new User();
        $e->setUseEmail($_REQUEST["email"]);
        $e->setUsePassword($_REQUEST["password"]);
        $r = $m->getById($e);
        echo json_encode($r);
    }

}
