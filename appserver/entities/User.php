<?php
class User extends BasicEntity implements JsonSerializable, IEntity {
/* Attributes */
/* @PrimaryKey */
	protected $useId;
	protected $useEmail;
	protected $usePassword;
	protected $useName;
/* Getters */
	public function getUseId(){
		return $this->useId;
	}

	public function getUseEmail(){
		return $this->useEmail;
	}

	public function getUsePassword(){
		return $this->usePassword;
	}

	public function getUseName(){
		return $this->useName;
	}

	public function getId(){
		return $this->getUseId();
	}

/* Setters */
	public function setUseId($param){
$this->setId($param);
		$this->useId = $param;
	}

	public function setUseEmail($param){
		$this->useEmail = $param;
	}

	public function setUsePassword($param){
		$this->usePassword = $param;
	}

	public function setUseName($param){
		$this->useName = $param;
	}

public function jsonSerialize() {
        $this->id = $this->useId;
        return get_object_vars($this);
        }
        
        public function lazyLoad() {
        }
        
        /*public function serializeByArray($array) {
            foreach ($array as $key => $value) {
                $this->{"{$key}"} = $value;
            }
        }

        public function serializeByObject($o) {
            foreach ($o as $key => $value) {
                $this->{"{$key}"} = $value;
            }
        }*/
}