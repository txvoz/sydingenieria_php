<?php
class Cert_product extends BasicEntity implements JsonSerializable, IEntity {
/* Attributes */
/* @PrimaryKey */
	protected $cpId;
/* @Index */
	protected $fkproduct = null;
	protected $prdId;
/* @Index */
	protected $fkcert = null;
	protected $cerId;
/* Getters */
	public function getCpId(){
		return $this->cpId;
	}

	public function getPrdId(){
		return $this->prdId;
	}

/** Index **/
	public function getFkProduct(){
if($this->fkproduct===null){$model = new productModel();$e = new Product();$e->setPrdId($this->prdId);$r = $model->getById($e);if($r->status===200){$this->fkproduct = $model->getById($e)->data;}}		return $this->fkproduct;
	}

	public function getCerId(){
		return $this->cerId;
	}

/** Index **/
	public function getFkCert(){
if($this->fkcert===null){$model = new certModel();$e = new Cert();$e->setCerId($this->cerId);$r = $model->getById($e);if($r->status===200){$this->fkcert = $model->getById($e)->data;}}		return $this->fkcert;
	}

	public function getId(){
		return $this->getCpId();
	}

/* Setters */
	public function setCpId($param){
$this->setId($param);
		$this->cpId = $param;
	}

	public function setPrdId($param){
		$this->prdId = $param;
	}

	public function setCerId($param){
		$this->cerId = $param;
	}

public function jsonSerialize() {
        $this->id = $this->cpId;
        return get_object_vars($this);
        }
        
        public function lazyLoad() {
        $this->getFkProduct();$this->getFkCert();}
        
        /*public function serializeByArray($array) {
            foreach ($array as $key => $value) {
                $this->{"{$key}"} = $value;
            }
        }

        public function serializeByObject($o) {
            foreach ($o as $key => $value) {
                $this->{"{$key}"} = $value;
            }
        }*/
}