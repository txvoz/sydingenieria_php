<?php
class Service extends BasicEntity implements JsonSerializable, IEntity {
/* Attributes */
/* @PrimaryKey */
	protected $serId;
	protected $serName;
	protected $serDescription;
/* Getters */
	public function getSerId(){
		return $this->serId;
	}

	public function getSerName(){
		return $this->serName;
	}

	public function getSerDescription(){
		return $this->serDescription;
	}

	public function getId(){
		return $this->getSerId();
	}

/* Setters */
	public function setSerId($param){
$this->setId($param);
		$this->serId = $param;
	}

	public function setSerName($param){
		$this->serName = $param;
	}

	public function setSerDescription($param){
		$this->serDescription = $param;
	}

public function jsonSerialize() {
        $this->id = $this->serId;
        return get_object_vars($this);
        }
        
        public function lazyLoad() {
        }
        
        /*public function serializeByArray($array) {
            foreach ($array as $key => $value) {
                $this->{"{$key}"} = $value;
            }
        }

        public function serializeByObject($o) {
            foreach ($o as $key => $value) {
                $this->{"{$key}"} = $value;
            }
        }*/
}