<?php

class Project extends BasicEntity implements JsonSerializable, IEntity {
    /* Attributes */
    /* @PrimaryKey */

    protected $pjtId;
    protected $pjtName;
    protected $pjtDescription;
    protected $pjtImagen;
    protected $pjtFinalized;

    /* Getters */

    public function getPjtId() {
        return $this->pjtId;
    }

    public function getPjtName() {
        return $this->pjtName;
    }

    public function getPjtDescription() {
        return $this->pjtDescription;
    }

    public function getPjtImagen() {
        return $this->pjtImagen;
    }

    public function getPjtFinalized() {
        return $this->pjtFinalized;
    }

    public function getId() {
        return $this->getPjtId();
    }

    /* Setters */

    public function setPjtId($param) {
        $this->setId($param);
        $this->pjtId = $param;
    }

    public function setPjtName($param) {
        $this->pjtName = $param;
    }

    public function setPjtDescription($param) {
        $this->pjtDescription = $param;
    }

    public function setPjtImagen($param) {
        $this->pjtImagen = $param;
    }

    public function setPjtFinalized($param) {
        $this->pjtFinalized = $param;
    }

    public function jsonSerialize() {
         $c = Config::singleton();
         if($this->getPjtImagen()!==""){
             $this->url = "{$c->get("cuploads")}/project/{$this->getPjtImagen()}";
         }else{
             $this->url = "";
         }
        
        $this->id = $this->pjtId;
        return get_object_vars($this);
    }

    public function lazyLoad() {
        
    }

    /* public function serializeByArray($array) {
      foreach ($array as $key => $value) {
      $this->{"{$key}"} = $value;
      }
      }

      public function serializeByObject($o) {
      foreach ($o as $key => $value) {
      $this->{"{$key}"} = $value;
      }
      } */
}
