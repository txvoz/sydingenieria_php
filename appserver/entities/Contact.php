<?php
class Contact extends BasicEntity implements JsonSerializable, IEntity {
/* Attributes */
/* @PrimaryKey */
	protected $conId;
	protected $conSubject;
	protected $conMessage;
	protected $conName;
	protected $conEmail;
	protected $conPhone;
/* Getters */
	public function getConId(){
		return $this->conId;
	}

	public function getConSubject(){
		return $this->conSubject;
	}

	public function getConMessage(){
		return $this->conMessage;
	}

	public function getConName(){
		return $this->conName;
	}

	public function getConEmail(){
		return $this->conEmail;
	}

	public function getConPhone(){
		return $this->conPhone;
	}

	public function getId(){
		return $this->getConId();
	}

/* Setters */
	public function setConId($param){
$this->setId($param);
		$this->conId = $param;
	}

	public function setConSubject($param){
		$this->conSubject = $param;
	}

	public function setConMessage($param){
		$this->conMessage = $param;
	}

	public function setConName($param){
		$this->conName = $param;
	}

	public function setConEmail($param){
		$this->conEmail = $param;
	}

	public function setConPhone($param){
		$this->conPhone = $param;
	}

public function jsonSerialize() {
        $this->id = $this->conId;
        return get_object_vars($this);
        }
        
        public function lazyLoad() {
        }
        
        /*public function serializeByArray($array) {
            foreach ($array as $key => $value) {
                $this->{"{$key}"} = $value;
            }
        }

        public function serializeByObject($o) {
            foreach ($o as $key => $value) {
                $this->{"{$key}"} = $value;
            }
        }*/
}