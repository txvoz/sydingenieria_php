-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 17, 2020 at 12:10 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sydingenieria`
--

-- --------------------------------------------------------

--
-- Table structure for table `allying`
--

CREATE TABLE `allying` (
  `allId` int(11) NOT NULL,
  `allName` varchar(45) COLLATE utf8_bin NOT NULL,
  `allDescription` text COLLATE utf8_bin NOT NULL,
  `allLogo` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `allContact` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `allEmail` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `allAddress` varchar(45) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `allying`
--

INSERT INTO `allying` (`allId`, `allName`, `allDescription`, `allLogo`, `allContact`, `allEmail`, `allAddress`) VALUES
(4, 'InnovaSolutions S.A.S.', 'Empresa de soluciones informaticas', 'y5feajZK1dXz5LY.png', 'William Rodriguez', 'william.rodriguez@innova.com', 'Calle 55 / Popayan'),
(5, 'test', 'adasdasd', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `cert`
--

CREATE TABLE `cert` (
  `cerId` int(11) NOT NULL,
  `cerTitle` varchar(45) COLLATE utf8_bin NOT NULL,
  `cerCode` varchar(45) COLLATE utf8_bin NOT NULL,
  `cerDescription` text COLLATE utf8_bin NOT NULL,
  `cerFile` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `cert`
--

INSERT INTO `cert` (`cerId`, `cerTitle`, `cerCode`, `cerDescription`, `cerFile`) VALUES
(4, 'test', 'test', 'test', 'XDiL0kvpuLxK6nB.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `cert_product`
--

CREATE TABLE `cert_product` (
  `cpId` int(11) NOT NULL,
  `prdId` int(11) NOT NULL,
  `cerId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `cert_product`
--

INSERT INTO `cert_product` (`cpId`, `prdId`, `cerId`) VALUES
(1, 14, 4);

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `conId` int(11) NOT NULL,
  `conSubject` varchar(45) COLLATE utf8_bin NOT NULL,
  `conMessage` text COLLATE utf8_bin NOT NULL,
  `conName` varchar(30) COLLATE utf8_bin NOT NULL,
  `conEmail` varchar(45) COLLATE utf8_bin NOT NULL,
  `conPhone` varchar(45) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`conId`, `conSubject`, `conMessage`, `conName`, `conEmail`, `conPhone`) VALUES
(1, 'Test', 'asdsad', 'test', 'test', '');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_item`
--

CREATE TABLE `gallery_item` (
  `gtmId` int(11) NOT NULL,
  `gtmTitle` varchar(45) COLLATE utf8_bin NOT NULL,
  `gtmDescription` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `gtmImage` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `gallery_item`
--

INSERT INTO `gallery_item` (`gtmId`, `gtmTitle`, `gtmDescription`, `gtmImage`) VALUES
(10, 'adasd', 'asdads', 'Kor09BwNeyxPltc.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `prdId` int(11) NOT NULL,
  `prdName` varchar(30) COLLATE utf8_bin NOT NULL,
  `prdDescription` text COLLATE utf8_bin NOT NULL,
  `prdImagen` varchar(100) COLLATE utf8_bin NOT NULL,
  `tpId` int(11) NOT NULL,
  `proId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`prdId`, `prdName`, `prdDescription`, `prdImagen`, `tpId`, `proId`) VALUES
(14, 'Producto 1', 'asdad', 'brWrJLrEwlN8KCD.jpg', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `pjtId` int(11) NOT NULL,
  `pjtName` varchar(45) COLLATE utf8_bin NOT NULL,
  `pjtDescription` text COLLATE utf8_bin NOT NULL,
  `pjtImagen` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `pjtFinalized` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`pjtId`, `pjtName`, `pjtDescription`, `pjtImagen`, `pjtFinalized`) VALUES
(4, 'asdasd', 'assdad', 'Ji5uiv5OSZRXdwZ.jpg', 0),
(5, 'adasd', 'adasd', 'rUVLYUApztsKUHp.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `provider`
--

CREATE TABLE `provider` (
  `proId` int(11) NOT NULL,
  `proNit` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `proName` varchar(45) COLLATE utf8_bin NOT NULL,
  `proDescription` text COLLATE utf8_bin NOT NULL,
  `proLogo` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `proWebSite` varchar(45) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `provider`
--

INSERT INTO `provider` (`proId`, `proNit`, `proName`, `proDescription`, `proLogo`, `proWebSite`) VALUES
(1, '1111', 'Provedor 1', 'Este proveedor es un generador de dinero a nivel internacional por que... ', NULL, ''),
(2, 'adasd', 'adad', 'asdad', 'imYept5v3DXa5bJ.jpg', 'http://adasd.com'),
(3, '11111', 'test', 'test', 'rxOAx6c0waabPP6.jpg', 'http://misitio.com');

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `serId` int(11) NOT NULL,
  `serName` varchar(45) COLLATE utf8_bin NOT NULL,
  `serDescription` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`serId`, `serName`, `serDescription`) VALUES
(1, 'Instalacion de paneles solares', 'Se instalan los paneles solares etc... '),
(2, 'Montaje de asdasd', 'adasd');

-- --------------------------------------------------------

--
-- Table structure for table `type_product`
--

CREATE TABLE `type_product` (
  `tpId` int(11) NOT NULL,
  `tpName` varchar(20) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `type_product`
--

INSERT INTO `type_product` (`tpId`, `tpName`) VALUES
(2, 'Chip'),
(1, 'Lampara Solar Cambio');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `useId` int(11) NOT NULL,
  `useEmail` varchar(45) COLLATE utf8_bin NOT NULL,
  `usePassword` varchar(45) COLLATE utf8_bin NOT NULL,
  `useName` varchar(30) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`useId`, `useEmail`, `usePassword`, `useName`) VALUES
(1, 'garodriguez335@misena.edu.co', '123', 'Gustavo Adolfo Rodriguez');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `allying`
--
ALTER TABLE `allying`
  ADD PRIMARY KEY (`allId`);

--
-- Indexes for table `cert`
--
ALTER TABLE `cert`
  ADD PRIMARY KEY (`cerId`);

--
-- Indexes for table `cert_product`
--
ALTER TABLE `cert_product`
  ADD PRIMARY KEY (`cpId`),
  ADD KEY `prdId` (`prdId`),
  ADD KEY `serId` (`cerId`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`conId`);

--
-- Indexes for table `gallery_item`
--
ALTER TABLE `gallery_item`
  ADD PRIMARY KEY (`gtmId`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`prdId`),
  ADD KEY `tpId` (`tpId`),
  ADD KEY `tpId_2` (`tpId`),
  ADD KEY `proId` (`proId`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`pjtId`);

--
-- Indexes for table `provider`
--
ALTER TABLE `provider`
  ADD PRIMARY KEY (`proId`),
  ADD UNIQUE KEY `proNit` (`proNit`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`serId`);

--
-- Indexes for table `type_product`
--
ALTER TABLE `type_product`
  ADD PRIMARY KEY (`tpId`),
  ADD UNIQUE KEY `tpName` (`tpName`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`useId`),
  ADD UNIQUE KEY `useEmail` (`useEmail`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `allying`
--
ALTER TABLE `allying`
  MODIFY `allId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `cert`
--
ALTER TABLE `cert`
  MODIFY `cerId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cert_product`
--
ALTER TABLE `cert_product`
  MODIFY `cpId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `conId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `gallery_item`
--
ALTER TABLE `gallery_item`
  MODIFY `gtmId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `prdId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `pjtId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `provider`
--
ALTER TABLE `provider`
  MODIFY `proId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `serId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `type_product`
--
ALTER TABLE `type_product`
  MODIFY `tpId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `useId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cert_product`
--
ALTER TABLE `cert_product`
  ADD CONSTRAINT `fk_cert_product_cert` FOREIGN KEY (`cerId`) REFERENCES `cert` (`cerId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cert_product_product` FOREIGN KEY (`prdId`) REFERENCES `product` (`prdId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `fk_product_provider` FOREIGN KEY (`proId`) REFERENCES `provider` (`proId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_type_product` FOREIGN KEY (`tpId`) REFERENCES `type_product` (`tpId`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
